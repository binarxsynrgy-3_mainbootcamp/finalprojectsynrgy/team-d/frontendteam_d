const SOCIAL_MEDIA = [
  {
    src: '/images/instagram.png',
  },
  {
    src: '/images/linkedin.png',
  },
  {
    src: '/images/twitter.png',
  },
  {
    src: '/images/facebook.png',
  },
  {
    src: '/images/youtube.png',
  },
];

export default SOCIAL_MEDIA;
