export const ASSESMENT_QUESTIONS = [
  {
    question:
      'Berikut ini adalah pernyataan yang benar. Diketahui, B : bilangan bulat, P : bilangan pecahan.',
    correctAnswer: 'd',
    options: {
      a: 'B * B = P',
      b: 'P * P = B',
      c: 'P – B = B',
      d: 'P / B = P',
    },
  },
  {
    question: 'Tampilkan Data Dosen yang memiliki jabatan dosen tetap',
    correctAnswer: 'a',
    options: {
      a: 'SELECT * FROM Dosen WHERE Jabatan="Dosen Tetap" ',
      b: 'SELECT TDosen.* FROM TDosen WHERE TDosen.Jabatan="Dosen Tetap" ',
      c: 'VIEW * FROM Dosen WHERE Jabatan="Dosen Tetap" ',
      d: 'SELECT * FROM Data_Dosen WHERE Jabatan="Dosen Tetap"',
    },
  },
  {
    question: 'Berikut adalah singkatan dari SQL, yaitu :',
    correctAnswer: 'b',
    options: {
      a: 'Standard Query Language ',
      b: 'Structured Query Language ',
      c: 'Simulator Query Language ',
      d: 'Structured Query Linear',
    },
  },
  {
    question:
      'DDL (Data Definition Language) merupakan perintah yang digunakan untuk membuat, mengubah dan menghapus objek database, meliputi:',
    correctAnswer: 'd',
    options: {
      a: 'Insert, Update, Drop ',
      b: 'Insert, Update, Delete ',
      c: 'Create, Alter, Delete ',
      d: 'Create, Alter, Drop',
    },
  },
  {
    question:
      'Berikut adalah contoh mendeklarasikan variabel dalam bahasa PHP, kecuali :',
    correctAnswer: 'd',
    options: {
      a: '$bil_bulat = 123; ',
      b: '$nama = "Yuli"; ',
      c: '$boolean = TRUE; ',
      d: '$bil pecahan = 0,5;',
    },
  },
];
