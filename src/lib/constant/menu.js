import {
  BellIcon,
  BookOpenIcon,
  CashIcon,
  CogIcon,
  HomeIcon,
  LogoutIcon,
} from '@heroicons/react/outline';

export const BORROWER_SIDEBAR = [
  {
    title: 'Dashboard',
    icon: HomeIcon,
    href: '/',
  },
  {
    title: 'Notifikasi',
    icon: BellIcon,
    href: '/notification',
  },
  {
    title: 'Kelas',
    icon: BookOpenIcon,
    href: '/class',
  },
  {
    title: 'Pinjaman',
    icon: CashIcon,
    href: '/loan',
  },
];

export const LENDER_SIDEBAR = [
  {
    title: 'Dashboard',
    icon: HomeIcon,
    href: '/',
  },
  {
    title: 'Notifikasi',
    icon: BellIcon,
    href: '/notification',
  },
  {
    title: 'Pengaturan',
    icon: CogIcon,
    href: '/loan',
  },
  {
    title: 'Keluar',
    icon: LogoutIcon,
    href: 'logout',
  },
];

export const NAVBAR_PUBLIC = [
  // {
  //   label: 'Pendanaan',
  //   route: '/pendanaan',
  // },
  // {
  //   label: 'Bootcamp',
  //   route: '/bootcamp',
  // },
  // {
  //   label: 'Cara Kerja',
  //   route: '/cara-kerja',
  // },
  // {
  //   label: 'Tentang',
  //   route: '/tentang',
  // },
];

10 * 10;
1.2 * 1.8;

export const NAVBAR_LENDER = [
  {
    label: 'Dashboard',
    route: '/dashboard-lender',
  },
  {
    label: 'Notifikasi',
    route: '/notification-lender',
  },
];
