import { useCallback, useEffect, useState } from 'react';

export const useGetStorage = (localKey, initialValue) => {
  const initialize = useCallback(
    (key) => {
      try {
        const item = localStorage.getItem(key);
        if (item && item !== 'undefined') {
          return JSON.parse(item);
        }
        return initialValue;
      } catch {
        return initialValue;
      }
    },
    [initialValue],
  );

  const [state, setState] = useState(null); // problem is here

  useEffect(() => {
    setState(initialize(localKey));
  }, [localKey, initialize]);

  return state;
};

export const getLocalStorageValue = (s) => JSON.parse(localStorage.getItem(s));

export const setLocalStorageValue = (s, v) => {
  localStorage.setItem(s, JSON.stringify(v));
};

export const removeLocalStorageValue = (s) => {
  localStorage.removeItem(s);
};
