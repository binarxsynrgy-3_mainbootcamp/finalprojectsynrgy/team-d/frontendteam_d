import { alertError } from '../hooks/toast';

const API_ENDPOINT = process.env.NEXT_PUBLIC_API;
const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

function AuthRepository() {
  const emailAvailability = async () => {
    const response = await fetch(`${API_ENDPOINT}`, {
      method: 'GET',
    });
    const data = await response.json();
    return data;
  };

  const refreshToken = async () => {
    const response = await fetch(API_ENDPOINT, {
      method: 'GET',
    });
    const data = await response.json();
    return data;
  };

  const register = async (payload) => {
    try {
      const response = await fetch(`${API_ENDPOINT}register`, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers,
      });
      const data = await response.json();
      if (data.code === 400) {
        alertError(data.message);
        return false;
      }
      return data;
    } catch (error) {
      alertError(error.message);
      return false;
    }
  };

  const signin = async (payload) => {
    try {
      const response = await fetch(`${API_ENDPOINT}signin`, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers,
      });
      const data = await response.json();
      if (data.code === 400) {
        alertError(data.message);
        return false;
      }
      return data;
    } catch (error) {
      alertError(error.message);
      return false;
    }
  };

  const verify = async (payload) => {
    try {
      const response = await fetch(`${API_ENDPOINT}signup/otp/verify`, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers,
      });
      const data = await response.json();
      if (data.code === 400) {
        alertError(data.message);
        return false;
      }
      return data;
    } catch (error) {
      alertError(error.message);
      return false;
    }
  };

  const signout = async () => {
    const response = await fetch(API_ENDPOINT, {
      method: 'GET',
    });
    const data = await response.json();
    return data;
  };

  return {
    emailAvailability,
    refreshToken,
    register,
    signin,
    signout,
    verify,
  };
}

export default AuthRepository;
