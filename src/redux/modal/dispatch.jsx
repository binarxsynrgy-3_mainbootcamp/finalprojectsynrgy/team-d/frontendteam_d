import { useDispatch, useSelector } from 'react-redux';
import { hide, show } from './slices';

const useModalDispatch = () => {
  const dispatch = useDispatch();
  const showModal = useSelector((state) => state.modal.showModal);

  const doShowModal = () => {
    dispatch(show());
  };

  const doHideModal = () => {
    dispatch(hide());
  };

  return {
    showModal,
    doShowModal,
    doHideModal,
  };
};

export default useModalDispatch;
