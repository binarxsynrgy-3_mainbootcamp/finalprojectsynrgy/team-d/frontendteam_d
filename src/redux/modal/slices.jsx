import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  showModal: false,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    hide: () => initialState,
    show(state) {
      Object.assign(state, {
        showModal: true,
      });
    },
  },
});

export const { hide, show } = modalSlice.actions;
export default modalSlice.reducer;
