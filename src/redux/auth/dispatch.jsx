import { useDispatch, useSelector } from 'react-redux';
import { setLogin, logout, setUser } from './slices';

const useAuthDispatch = () => {
  const dispatch = useDispatch();
  const isLogged = useSelector((state) => state.auth.isLogged);
  const user = useSelector((state) => state.auth.user);

  const doLogout = () => {
    dispatch(logout());
  };

  const doLogin = (data) => {
    dispatch(setUser(data));
    dispatch(setLogin());
  };

  return {
    isLogged,
    user,
    doLogout,
    doLogin,
  };
};

export default useAuthDispatch;
