import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLogged: false,
  user: {
    roles: ['visitor'],
  },
};

const authSlices = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: () => initialState,
    setLogin(state) {
      Object.assign(state, {
        isLogged: true,
      });
    },
    setUser(state, actions) {
      Object.assign(state, {
        ...state,
        user: actions.payload,
      });
    },
  },
});

export const { logout, setLogin, setUser } = authSlices.actions;
export default authSlices.reducer;
