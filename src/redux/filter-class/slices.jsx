import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  academy: [],
  interest: [],
};

const filterClassSlices = createSlice({
  name: 'filterClass',
  initialState,
  reducers: {
    resetFilter: () => initialState,
    setFilter(state, actions) {
      Object.assign(state, {
        ...state,
        ...actions.payload,
      });
    },
  },
});

export const { resetFilter, setFilter } = filterClassSlices.actions;
export default filterClassSlices.reducer;
