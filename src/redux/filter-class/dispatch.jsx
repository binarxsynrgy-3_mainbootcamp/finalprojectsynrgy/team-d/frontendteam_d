import { useDispatch, useSelector } from 'react-redux';
import { resetFilter, setFilter } from './slices';

const useFilterClass = () => {
  const dispatch = useDispatch();
  const filterClass = useSelector((state) => state.filterClass);

  const doResetFilter = () => {
    dispatch(resetFilter());
  };

  const applyFilter = (data) => {
    dispatch(setFilter(data));
  };

  return {
    filterClass,
    applyFilter,
    doResetFilter,
  };
};

export default useFilterClass;
