import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import spinner from './spinner/slices';
import auth from './auth/slices';
import filterClass from './filter-class/slices';
import assesment from './assesment/slices';
import modal from './modal/slices';

const rootReducers = combineReducers({
  spinner,
  auth,
  filterClass,
  assesment,
  modal,
});

const store = configureStore({
  reducer: rootReducers,
});

export default store;
