import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  testScores: 0,
  isDone: false,
  workDate: '',
  startTime: null,
};

const assesmentSlice = createSlice({
  name: 'assesement',
  initialState,
  reducers: {
    clear: () => initialState,
    defaultValue(state) {
      Object.assign(state, {
        ...state,
        ...initialState,
      });
    },
    setValues(state, actions) {
      Object.assign(state, {
        ...state,
        ...actions.payload,
      });
    },
    startTime(state, actions) {
      Object.assign(state, {
        ...state,
        startTime: actions.payload,
      });
    },
  },
});

export const { clear, setValues, defaultValue, startTime } =
  assesmentSlice.actions;
export default assesmentSlice.reducer;
