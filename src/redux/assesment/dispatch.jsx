import { useDispatch, useSelector } from 'react-redux';
import { clear, setValues, defaultValue, startTime } from './slices';

const useAssementDispatch = () => {
  const dispatch = useDispatch();
  const assesment = useSelector((state) => state.assesment);

  const defaultAssesment = () => {
    dispatch(defaultValue());
  };

  const clearAssement = () => {
    dispatch(clear());
  };

  const setAssement = (data) => {
    dispatch(setValues(data));
  };

  const setStartTime = (time) => {
    dispatch(startTime(time));
  };

  return {
    assesment,
    setAssement,
    clearAssement,
    defaultAssesment,
    setStartTime,
  };
};

export default useAssementDispatch;
