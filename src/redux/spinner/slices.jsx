import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  showSpinner: false,
};

const spinnerSlices = createSlice({
  name: 'spinner',
  initialState,
  reducers: {
    hide: () => initialState,
    show(state) {
      Object.assign(state, {
        showSpinner: true,
      });
    },
  },
});

export const { hide, show } = spinnerSlices.actions;
export default spinnerSlices.reducer;
