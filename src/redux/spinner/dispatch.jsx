import { useDispatch, useSelector } from 'react-redux';
import { hide, show } from './slices';

const useSpinnerDispatch = () => {
  const dispatch = useDispatch();
  const showSpinner = useSelector((state) => state.spinner.showSpinner);

  const doShowSpinner = () => {
    dispatch(show());
  };

  const doHideSpinner = () => {
    dispatch(hide());
  };

  return {
    showSpinner,
    doShowSpinner,
    doHideSpinner,
  };
};

export default useSpinnerDispatch;
