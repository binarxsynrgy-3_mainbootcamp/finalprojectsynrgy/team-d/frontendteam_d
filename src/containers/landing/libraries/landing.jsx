import {
  Box,
  Button,
  Container,
  Flex,
  Heading,
  Image,
  SimpleGrid,
  Stack,
  Text,
} from '@chakra-ui/react';
import SOCIAL_MEDIA from '@/lib/constant/sosmed';

const Feature = ({ title, text, icon }) => (
  <Stack alignItems="center">
    <Box position="relative">
      <Image
        alt="Hero Image"
        fit="cover"
        align="center"
        w="100%"
        h="100%"
        src={icon}
      />
    </Box>
    <h1 className="text-2xl font-bold text-center items-center text-cafund-primary">
      {title}
    </h1>
    <Text color="gray.600" textAlign="center">
      {text}
    </Text>
  </Stack>
);

const LandingContainer = () => (
  <div>
    <Container maxW="7xl">
      <Stack
        align="center"
        spacing={{ base: 8, md: 10 }}
        mt={{ base: 0, md: 18 }}
        mb={{ base: 16, md: 28 }}
        direction={{ base: 'column', md: 'row' }}
      >
        <Stack flex={1} spacing={{ base: 5, md: 10 }}>
          <Heading
            lineHeight={1.1}
            fontWeight={700}
            fontSize={{ base: '3xl', sm: '4xl', lg: '5xl' }}
          >
            <Text as="span">Solusi Pinjaman Dana untuk Bootcamp Kamu</Text>
          </Heading>
          <Text color="gray.500">
            Di CareerFund kamu tidak perlu khawatir akan biaya bootcamp yang
            mahal. Kamu bisa membayar cicilan yang ringan setiap bulannya.
          </Text>
          <Stack
            spacing={{ base: 4, sm: 6 }}
            direction={{ base: 'column', sm: 'row' }}
          >
            <Button
              rounded="md"
              size="lg"
              fontWeight="normal"
              px={8}
              colorScheme="cafund"
            >
              Daftar Sekarang
            </Button>
          </Stack>
        </Stack>
        <Flex
          flex={1}
          justify="center"
          align="center"
          position="relative"
          w="full"
        >
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="100%"
              h="100%"
              src="/images/hero-1.png"
            />
          </Box>
        </Flex>
      </Stack>
    </Container>
    <div className="m-auto bg-gray-100 py-8">
      <Container maxW="7xl">
        <Stack
          align="center"
          spacing={{ base: 8, md: 10 }}
          mt={{ base: 0, md: 18 }}
          mb={{ base: 16, md: 28 }}
          direction={{ base: 'column', md: 'row' }}
        >
          <Flex
            flex={1}
            justify="center"
            align="center"
            position="relative"
            w="full"
          >
            <Box position="relative">
              <Image
                alt="Hero Image"
                fit="cover"
                align="center"
                w="100%"
                h="100%"
                src="/images/hero-2.png"
              />
            </Box>
          </Flex>
          <Stack flex={1} spacing={{ base: 5, md: 10 }}>
            <Heading
              lineHeight={1.1}
              fontWeight={700}
              fontSize={{ base: '3xl', sm: '4xl', lg: '5xl' }}
            >
              <Text as="span">Danai Pembelajaran Tanpa Takut Uang Hilang</Text>
            </Heading>
            <Text color="gray.500">
              Di CareerFund kamu tidak perlu khawatir pendanaanmu hilang, dana
              akan langsung dibayarkan ke penyelenggara bootcamp.
            </Text>
            <Stack
              spacing={{ base: 4, sm: 6 }}
              direction={{ base: 'column', sm: 'row' }}
            >
              <Button
                rounded="md"
                size="lg"
                fontWeight="normal"
                px={8}
                colorScheme="cafund"
              >
                Danai Sekarang
              </Button>
            </Stack>
          </Stack>
        </Stack>
      </Container>
    </div>
    <Container maxW="7xl" my="16">
      <Stack>
        <div className="text-center my-8">
          <h2 className="text-5xl font-bold">Kenapa CareerFund?</h2>
        </div>
        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
          <Feature
            icon="/images/illustrasi-1.png"
            title="Bayar Bootcamp dengan Fleksibel"
            text="Kamu bisa mengatur uang muka, dan periode cicilan sesuai dengan kebutuhan kamu."
          />
          <Feature
            icon="/images/illustrasi-2.png"
            title="Pilih Peminjam dengan Mudah"
            text="Kamu bisa memilih peminjam yang berpotensi menyelesaikan bootcamp dengan mudah."
          />
          <Feature
            icon="/images/illustrasi-3.png"
            title="Jaminan Pendanaan Aman"
            text="Dana yang dibayarkan akan langsung disalurkan ke penyelenggara bootcamp."
          />
        </SimpleGrid>
      </Stack>
    </Container>
    <div className="m-auto bg-gray-100 py-8">
      <Container maxW="7xl">
        <Stack p={4}>
          <div className="text-center my-16">
            <h2 className="text-5xl font-bold">
              Simulasikan Cicilan atau Keuntunganmu
            </h2>
          </div>
          <Flex justifyContent="center">
            <SimpleGrid maxW="5xl" columns={{ base: 1, md: 2 }} spacing={10}>
              <Feature
                icon="/images/illustrasi-4.png"
                title="Cicil Bootcamp Keinginanmu"
                text="Pilih bootcamp yang kamu inginkan lalu tentukan uang mukanya dan bayar dengan cicilan ringan setiap bulannya."
              />
              <Feature
                icon="/images/illustrasi-5.png"
                title="Danai Bootcamp Potensial"
                text="Danai peserta bootcamp yang memiliki potensi tinggi menyelesaikan pembelajaran hingga akhir bootcamp."
              />
            </SimpleGrid>
          </Flex>
        </Stack>
      </Container>
    </div>
    <Container maxW="7xl" my="16">
      <Stack
        align="center"
        spacing={{ base: 8, md: 10 }}
        mt={{ base: 0, md: 18 }}
        mb={{ base: 16, md: 28 }}
        direction={{ base: 'column', md: 'row' }}
      >
        <Flex
          flex={1}
          justify="center"
          align="center"
          position="relative"
          w="full"
        >
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="100%"
              h="100%"
              src="/images/hero-3.png"
            />
          </Box>
        </Flex>
        <Stack flex={1} spacing={{ base: 5, md: 10 }}>
          <Heading
            lineHeight={1.1}
            fontWeight={700}
            fontSize={{ base: '3xl', sm: '4xl', lg: '5xl' }}
          >
            <span className="text-5xl">
              CareerFund hadir di Smartphone Kamu
            </span>
          </Heading>
          <p className="text-lg text-gray-500">
            Gunakan CareerFund di smartphone kamu dan dapatkan&nbsp;
            <span className="text-cafund-primary font-semibold">
              cashback s.d. 10%
            </span>
            &nbsp;untuk transaksi yang kamu lakukan. Pantau setiap aktivitas dan
            pembaruan pendanaan kamu dengan mudah.
          </p>
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="30%"
              h="30%"
              src="/images/appstore.png"
            />
          </Box>
        </Stack>
      </Stack>
    </Container>

    {/* Footer */}
    <div className="bg-cafund-primary m-auto justify-center flex p-8 text-white">
      <div className="max-w-7xl">
        <Stack
          align="center"
          spacing={{ base: 8, md: 10 }}
          mb="6"
          direction={{ base: 'column', md: 'row' }}
        >
          <div className="flex-1 space-y-6">
            <h1 className="text-xl font-bold">PT. Careerfund Benderang</h1>
            <p className="text-lg">
              JL. TB Simatupang No.1 Jakarta Selatan, Indonesia 12430 Office:
              021 123123 Customer Support: support@careerfund.com Business Hour:
              Monday - Friday, 9 AM - 5 PM
            </p>
          </div>
          <div className="flex-1 space-y-4">
            <div className="grid grid-cols-3">
              <div>Tentang Kami</div>
              <div>Hubungi Kami</div>
              <div>Syarat dan Ketentuan</div>
            </div>
            <div className="grid grid-cols-3">
              <div>Cara Kerja</div>
              <div>FAQs</div>
            </div>
            <div className="flex flex-row gap-4">
              {SOCIAL_MEDIA.map((item) => (
                <Image
                  key={item.src}
                  alt="Hero Image"
                  fit="cover"
                  align="center"
                  w="40px"
                  h="40px"
                  src={item.src}
                />
              ))}
            </div>
          </div>
        </Stack>
        <Stack align="center" direction={{ base: 'column', md: 'row' }}>
          <h1 className="text-xl font-bold">Disclaimer Risiko: </h1>
        </Stack>
        <Stack
          align="center"
          spacing={{ base: 8, md: 10 }}
          mt={{ base: 0, md: 18 }}
          mb={{ base: 16, md: 28 }}
          direction={{ base: 'column', md: 'row' }}
        >
          <Stack flex={1} spacing={{ base: 5, md: 6 }}>
            <div className="pl-6">
              <ul className="list-disc">
                <li className="text-justify">
                  Layanan Pinjam Meminjam Berbasis Teknologi Informasi merupakan
                  kesepakatan perdata antara Pemberi Pinjaman dengan Penerima
                  Pinjaman, sehingga segala risiko yang timbul dari kesepakatan
                  tersebut ditanggung sepenuhnya oleh masing-masing pihak.
                </li>
                <li className="text-justify">
                  Risiko kredit atau gagal bayar ditanggung sepenuhnya oleh
                  Pemberi Pinjaman. Tidak ada lembaga atau otoritas negara yang
                  bertanggung jawab atas risiko gagal bayar ini. Penyelenggara
                  dengan persetujuan dari masing-masing Pengguna (Pemberi
                  Pinjaman dan/atau Penerima Pinjaman) mengakses, memperoleh,
                  menyimpan, mengelola dan/atau menggunakan data pribadi
                  Pengguna (&quot;Pemanfaatan atau di dalam benda, perangkat
                  elektronik (termasuk smartphone atau telepon seluler),
                  perangkat keras (hardware) maupun lunak (software), dokumen
                  elektronik, aplikasi atau sistem elektronik milik Pengguna
                  atau yang dikuasai Pengguna, dengan memberitahukan tujuan,
                  batasan dan mekanisme Pemanfaatan Data tersebut kepada
                  Pengguna yang bersangkutan sebelum memperoleh persetujuan yang
                  dimaksud.
                </li>
                <li className="text-justify">
                  Pemberi Pinjaman yang belum memiliki pengetahuan dan
                  pengalaman pinjam meminjam, disarankan untuk tidak menggunakan
                  layanan ini.
                </li>
                <li className="text-justify">
                  Penerima Pinjaman harus mempertimbangkan tingkat bunga
                  pinjaman dan biaya lainnya sesuai dengan kemampuan dalam
                  melunasi pinjaman.
                </li>
              </ul>
            </div>
          </Stack>
          <Stack flex={1} spacing={{ base: 5, md: 10 }}>
            <div className="pl-6">
              <ul className="list-disc">
                <li className="text-justify">
                  Layanan Pinjam Meminjam Berbasis Teknologi Informasi merupakan
                  kesepakatan perdata antara Pemberi Pinjaman dengan Penerima
                  Pinjaman, sehingga segala risiko yang timbul dari kesepakatan
                  tersebut ditanggung sepenuhnya oleh masing-masing pihak.
                </li>
                <li className="text-justify">
                  Risiko kredit atau gagal bayar ditanggung sepenuhnya oleh
                  Pemberi Pinjaman. Tidak ada lembaga atau otoritas negara yang
                  bertanggung jawab atas risiko gagal bayar ini. Penyelenggara
                  dengan persetujuan dari masing-masing Pengguna (Pemberi
                  Pinjaman dan/atau Penerima Pinjaman) mengakses, memperoleh,
                  menyimpan, mengelola dan/atau menggunakan data pribadi
                  Pengguna (&quot;Pemanfaatan atau di dalam benda, perangkat
                  elektronik (termasuk smartphone atau telepon seluler),
                  perangkat keras (hardware) maupun lunak (software), dokumen
                  elektronik, aplikasi atau sistem elektronik milik Pengguna
                  atau yang dikuasai Pengguna, dengan memberitahukan tujuan,
                  batasan dan mekanisme Pemanfaatan Data tersebut kepada
                  Pengguna yang bersangkutan sebelum memperoleh persetujuan yang
                  dimaksud.
                </li>
                <li className="text-justify">
                  Pemberi Pinjaman yang belum memiliki pengetahuan dan
                  pengalaman pinjam meminjam, disarankan untuk tidak menggunakan
                  layanan ini.
                </li>
                <li className="text-justify">
                  Penerima Pinjaman harus mempertimbangkan tingkat bunga
                  pinjaman dan biaya lainnya sesuai dengan kemampuan dalam
                  melunasi pinjaman.
                </li>
              </ul>
            </div>
          </Stack>
        </Stack>
      </div>
    </div>
  </div>
);

export default LandingContainer;
