import { Box, Button, Link } from '@chakra-ui/react';
import { Formik } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import { useState, useEffect } from 'react';

import useSpinnerDispatch from '@/redux/spinner/dispatch';
import AuthRepository from '@/lib/repositories/auth';
import { alertSuccess } from '@/lib/hooks/toast';
import useAuthDispatch from '@/redux/auth/dispatch';

const validationSchema = Yup.object({
  code: Yup.string().required('OTP harus di isi'),
});

const initialValues = {
  code: '',
};

const OtpContainer = () => {
  const router = useRouter();
  const [emailDisplay, setEmailDisplay] = useState('');
  const { doHideSpinner, doShowSpinner, showSpinner } = useSpinnerDispatch();
  const { doLogin } = useAuthDispatch();

  useEffect(() => {
    if (!router.isReady) return;
    const { email } = router.query;
    setEmailDisplay(email);
  }, [router]);

  const handleOnSubmit = async (values) => {
    const { code } = values;
    doShowSpinner(true);
    const payload = {
      code,
    };
    const data = await AuthRepository().verify(payload);
    doHideSpinner(false);
    if (data) {
      alertSuccess('Kode OTP valid, anda akan di arahkan ke halaman dashboard');
      setTimeout(() => {
        doLogin(data);
        router.push('/');
      }, 3000);
    }
  };

  return (
    <div className="h-screen">
      <Box className="flex mx-auto justify-between my-24 px-16">
        <Box className="place-content-center w-96 m-auto">
          <h1 className="text-4xl text-center font-bold mb-4">
            Masukkan Kode OTP
          </h1>
          <p className="text-center mb-8">
            Kami telah mengirimkan kode OTP ke
            <Link
              href="/register"
              color="blue.500"
              fontWeight="bold"
              marginLeft="1"
            >
              {emailDisplay}
            </Link>
          </p>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
              values,
            }) => (
              <form
                className="form__container space-y-2"
                onSubmit={handleSubmit}
              >
                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="code">
                    <input
                      name="code"
                      id="code"
                      placeholder="Kode OTP"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.code}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.code && (
                      <span
                        className={`text-red-500 form__control-message ${
                          touched.code && errors.code ? 'error' : ''
                        }`}
                      >
                        {touched.code && errors.code && errors.code}
                      </span>
                    )}
                  </div>
                </div>
                <div className="form__group form__actions">
                  <div>
                    <Button
                      w="100%"
                      colorScheme="cafund"
                      px={8}
                      type="submit"
                      className="mt-16"
                      disabled={showSpinner}
                    >
                      {showSpinner ? 'Mohon tunggu...' : 'Lanjutkan'}
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
          <Box className="mt-8">
            <p className="mt-5 text-center">
              Tidak Menerima kode?
              <Link
                href="/register"
                color="blue.500"
                fontWeight="bold"
                marginLeft="1"
              >
                Kirim Ulang
              </Link>
            </p>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default OtpContainer;
