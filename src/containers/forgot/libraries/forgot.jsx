import { Box, Image } from '@chakra-ui/react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

const forgotContainer = () => {
  const ForgotSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Required'),
  });

  return (
    <div className="h-screen">
      <Box className="flex mx-auto justify-between my-24 px-20 pt-10">
        <Box className="ml=10 place-content-center w-96">
          <h1 className="pt-16 mb-16 text-4xl text-center font-bold">
            Lupa Sandi
          </h1>
          <Formik
            initialValues={{
              password: '',
              email: '',
            }}
            validationSchema={ForgotSchema}
            onSubmit={() => {}}
          >
            {({ errors, touched }) => (
              <Form>
                <div className="mt-6 mb-10">
                  <Field
                    name="email"
                    className="appearance-none border-b-2 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="email"
                    type="text"
                    placeholder="Email"
                  />
                  {errors.email && touched.email ? (
                    <p className="text-red-500 border-none">{errors.email}</p>
                  ) : null}
                </div>

                <button
                  className="bg-cafund-primary mt-6 mb-3 hover:bg-blue-700 text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full"
                  type="submit"
                >
                  Reset password
                </button>
              </Form>
            )}
          </Formik>
        </Box>

        <div className="flex flex-col items-center justify-center">
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="100%"
              h="100%"
              src="/img/Foto.png"
            />
          </Box>
        </div>
      </Box>
    </div>
  );
};

export default forgotContainer;
