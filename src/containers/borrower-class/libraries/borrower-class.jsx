import Alert from '@/components/Alert';
import { CLASS_LIST } from '@/lib/constant/class';
import { getLocalStorageValue, useGetStorage } from '@/lib/hooks/useStorage';
import useAssementDispatch from '@/redux/assesment/dispatch';
import useFilterClass from '@/redux/filter-class/dispatch';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';
import NumberFormat from 'react-number-format';
import FilterCard from './filter-card';

const ClassBox = ({ bootcamp, assessmentDone, handleRegister }) => {
  return (
    <div className="bg-white mt-7 rounded-lg">
      <div className="flex items-center flex-row justify-between w-full  p-8">
        <div className="flex flex-row gap-5">
          <Image
            width={65}
            height={65}
            className="inline-block"
            src="/images/logo-binar.png"
            alt=""
          />
          <div>
            <div className="text-xl font-bold mt-1">
              {bootcamp.bootcampName}
            </div>
            <div className="text-lg ">{bootcamp.className}</div>
          </div>
        </div>
        <div className="flex flex-row gap-8">
          <div>
            <div className="text-lg text-gray-500">Durasi Bootcamp</div>
            <div className="text-xl font-bold text-cafund-primary">
              {bootcamp.duration}
            </div>
          </div>
          <div>
            <div className="text-lg text-gray-500">Harga Bootcamp</div>
            <div className="text-xl font-bold text-cafund-primary">
              <NumberFormat
                value={bootcamp.price}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end pt-5 p-7">
        <button
          type="button"
          className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-4/12 disabled:bg-gray-500"
          onClick={handleRegister}
          disabled={!assessmentDone || bootcamp.isRegistered}
        >
          {bootcamp.isRegistered ? 'Telah Terdaftar' : 'Daftar'}
        </button>
      </div>
    </div>
  );
};

const ClassNotFound = () => {
  return (
    <div className="flex text-center overscroll-none m-auto">
      <div className="m-auto mt-24 w-3/5">
        <Image
          width={191}
          height={191}
          className="mx-40"
          src="/images/class-not-found.png"
          alt=""
        />
        <div className="mt-6 text-xl font-bold text-black">
          Yah, tidak ada kelas yang bisa diikuti
        </div>
      </div>
    </div>
  );
};

const MainBox = () => {
  const router = useRouter();
  const [classes, setClasses] = useState(CLASS_LIST);
  const [myClasses, setMyClasses] = useState([]);
  const { filterClass } = useFilterClass();
  const assesmentStorage = useGetStorage('assesment', false);
  const { assesment, defaultAssesment, setAssement } = useAssementDispatch();

  useEffect(() => {
    if (!assesmentStorage) {
      defaultAssesment();
    } else {
      setAssement(assesmentStorage);
    }
  }, [assesmentStorage, defaultAssesment]);

  const applyFilterClass = useCallback(() => {
    let filteredByAcademy = CLASS_LIST;
    if (filterClass.academy.length > 0) {
      filteredByAcademy = CLASS_LIST.filter((classItem) =>
        filterClass.academy.some((item) => classItem.id.includes(item)),
      );
    }

    if (filterClass.interest.length > 0) {
      const filteredByInterest = filteredByAcademy.filter((classItem) =>
        filterClass.interest.some((item) =>
          classItem.className.toLowerCase().includes(item),
        ),
      );
      setClasses(filteredByInterest);
    } else {
      setClasses(filteredByAcademy);
    }
  }, [filterClass]);

  useEffect(() => {
    if (filterClass.academy.length === 0 && filterClass.interest.length === 0) {
      setClasses(CLASS_LIST);
    } else {
      applyFilterClass();
    }
  }, [filterClass, setClasses, applyFilterClass]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        setMyClasses(myClass);
        classes.forEach((classItem) => {
          if (
            myClass.filter(
              (item) =>
                item.id === classItem.id &&
                item.className === classItem.className,
            ).length > 0
          ) {
            classItem.isRegistered = true;
          } else {
            classItem.isRegistered = false;
          }
        });
        if (classes.length > 10) {
          setClasses(classes.slice(0, 10));
        }
      }
    }
  }, []);

  const search = (e) => {
    const { value } = e.target;
    const newClass = CLASS_LIST.filter((item) => {
      const keyword = value.toLowerCase();
      const bootcampName = item.bootcampName.toLowerCase();
      const className = item.className.toLowerCase();
      return bootcampName.includes(keyword) || className.includes(keyword);
    });
    setClasses(newClass);
  };

  const handleRegister = (id, className) => {
    router.push({
      pathname: '/installment',
      query: {
        id,
        className,
      },
    });
  };

  return (
    <div className="flex-grow mt-6">
      <h1 className="text-4xl font-bold text-cafund-primary">Kelas</h1>
      <div className="flex flex-col  mx-auto text-black mt-6 gap-4">
        <input
          className="flex-grow  border-2 border-gray-200  bg-gray-200 h-10 px-5 rounded-lg focus:outline-none "
          type="search"
          name="search"
          placeholder="Cari nama lembaga atau bidang"
          onInput={search}
        />
        <div className="overflow-y-auto" style={{ maxHeight: '600px' }}>
          {classes && classes.length > 0 ? (
            classes.map((item, index) => (
              <div key={index}>
                <ClassBox
                  bootcamp={item}
                  index={index}
                  assessmentDone={assesment.isDone}
                  handleRegister={() => handleRegister(item.id, item.className)}
                />
              </div>
            ))
          ) : (
            <ClassNotFound />
          )}
        </div>
      </div>
    </div>
  );
};

const BorrowerClass = () => {
  const { assesment } = useAssementDispatch();

  return (
    <div className="dashboard-borrower h-full">
      {assesment && !assesment.isDone && (
        <Alert
          variant="error"
          message="Silahkan selesaikan asesmen anda sebelum melakukan pendaftaran!"
        />
      )}

      {assesment && assesment.isDone && assesment.testScores < 85 && (
        <Alert
          variant="error"
          message={`Nilai asesmen anda adalah ${assesment.testScores}, hal tersebut bisa menjadi pertimbangan oleh para pemberi pinjaman`}
        />
      )}

      <div className="flex flex-row gap-6 pb-8">
        <MainBox />
        <FilterCard />
      </div>
    </div>
  );
};

export default BorrowerClass;
