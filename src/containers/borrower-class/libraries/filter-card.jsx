import { ACADEMIES, INTEREST } from '@/lib/constant/class';
import useFilterClass from '@/redux/filter-class/dispatch';
import React, { useState } from 'react';

const FilterCard = () => {
  const [academies, setAcademies] = useState(ACADEMIES);
  const [interest, setInterest] = useState(INTEREST);
  const { filterClass, applyFilter } = useFilterClass();

  const onChangeAcademy = (id) => {
    const newAcademies = academies.map((item) => {
      if (item.id === id) {
        item.checked = !item.checked;
      }
      return item;
    });
    setAcademies(newAcademies);
  };

  const onChangeInterest = (id) => {
    const newInterest = interest.map((item) => {
      if (item.id === id) {
        item.checked = !item.checked;
      }
      return item;
    });
    setInterest(newInterest);
  };

  const onApplyFilter = () => {
    const keywordFilter = { ...filterClass };
    const filteredAcademies = academies.filter((item) => item.checked);
    if (filteredAcademies.length > 0) {
      const academy = filteredAcademies.map((item) => item.id);
      keywordFilter.academy = academy;
    } else {
      keywordFilter.academy = [];
    }

    const filteredInterest = interest.filter((item) => item.checked);
    if (filteredInterest.length > 0) {
      const interest = filteredInterest.map((item) => item.id);
      keywordFilter.interest = interest;
    } else {
      keywordFilter.interest = [];
    }
    applyFilter(keywordFilter);
  };

  const resetFilter = () => {
    const newInterest = interest.map((item) => {
      item.checked = false;
      return item;
    });
    const newAcademies = academies.map((item) => {
      item.checked = false;
      return item;
    });
    setAcademies(newAcademies);
    setInterest(newInterest);
    applyFilter({
      interest: [],
      academy: [],
    });
  };

  return (
    <div className="flex-grow-0">
      <div className="p-6 bg-white rounded-md right-0">
        <h1 className="text-xl font-bold mb-8">Filter</h1>
        <div className="mb-8">
          <p className="mb-1 text-lg">Penyelenggara Bootcamp</p>
          {academies &&
            academies.length > 0 &&
            academies.map((academy) => (
              <div key={academy.id} className="block">
                <label className="inline-flex items-center mt-2">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-cafund-primary"
                    value={academy.id}
                    checked={academy.checked}
                    onChange={() => onChangeAcademy(academy.id)}
                  />
                  <span className="ml-2 text-gray-700">{academy.name}</span>
                </label>
              </div>
            ))}
        </div>

        <div className="mb-8">
          <p className="mb-1 text-lg">Jenis Course </p>
          {interest &&
            interest.length > 0 &&
            interest.map((interestItem) => (
              <div key={interestItem.id} className="block">
                <label className="inline-flex items-center mt-2">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-cafund-primary"
                    value={interestItem.id}
                    checked={interestItem.checked}
                    onChange={() => onChangeInterest(interestItem.id)}
                  />
                  <span className="ml-2 text-gray-700">
                    {interestItem.name}
                  </span>
                </label>
              </div>
            ))}
        </div>

        <div className="mb-10">
          <p className="mb-2 text-lg">Harga Bootcamp</p>
          <div className="flex justify-between mx-auto text-black">
            <input
              className="w-20 border-2 text-center border-gray-200  bg-gray-200 h-8 px-3 rounded-lg focus:outline-none "
              type="min"
              name="min"
              placeholder="Min"
            />
            <hr className="w-10 my-auto border-1 bg-gray-300" />
            <input
              className="w-20 border-2 text-center border-gray-200  bg-gray-200 h-8 px-3 rounded-lg focus:outline-none "
              type="max"
              name="max"
              placeholder="Max"
            />
          </div>
        </div>

        <div className="flex flex-row gap-2 justify-between">
          <button
            type="button"
            className="w-full bg-cafund-primary text-white font-bold py-2 px-4 rounded-md"
            onClick={onApplyFilter}
          >
            Terapkan
          </button>
          <button
            type="button"
            className="w-full bg-cafund-base font-bold py-2 px-4 rounded-md"
            onClick={resetFilter}
          >
            Reset
          </button>
        </div>
      </div>
    </div>
  );
};

export default FilterCard;
