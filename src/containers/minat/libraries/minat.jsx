import Layout from '@/layouts/Layout';

const minatContainer = () => (
  <Layout>
    <div className="w-full container flex items-center mx-auto justify-center px-4">
      <main className="w-3/4 mx-auto justify-center">
        <div>
          <p className="mb-8 pb-8 text-3xl text-center font-bold ">
            Bidang apa yang ingin kamu pelajari?
          </p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-6 space-y-0">
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                top: -15,
                left: 15,
                zIndex: 3,
              }}
            >
              <img
                width={30}
                height={30}
                src="/img/icon.png"
                style={{
                  alignSelf: 'flex-center',
                }}
              />
            </div>
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#882424',
                borderWidth: 1,
                borderColor: '#0d78f2',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>
                Backend Development
              </p>
              <img
                width="45%"
                src="/img/minat1.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                left: 15,
                zIndex: 3,
                visibility: 'hidden',
              }}
            />
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#71277c',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>Digital Marketing</p>
              <img
                width="45%"
                src="/img/minat2.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                left: 15,
                zIndex: 3,
                visibility: 'hidden',
              }}
            />
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#1f526f',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>
                Android Development
              </p>
              <img
                width="45%"
                src="/img/minat3.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                left: 15,
                zIndex: 3,
                visibility: 'hidden',
              }}
            />
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#216b6e',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>UI UX Design</p>
              <img
                width="45%"
                src="/img/minat4.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                left: 15,
                zIndex: 3,
                visibility: 'hidden',
              }}
            />
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#26276a',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>IOS Development</p>
              <img
                width="45%"
                src="/img/minat5.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                left: 15,
                zIndex: 3,
                visibility: 'hidden',
              }}
            />
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#2a7c65',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>
                Front End Development
              </p>
              <img
                width="45%"
                src="/img/minat6.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
          <div className="box-border flex flex-col justify-between text-base font-bold text-white">
            <div
              style={{
                height: 30,
                width: 30,
                borderRadius: 20,
                backgroundColor: '#0d78f2',
                position: 'relative',
                alignSelf: 'flex-end',
                top: -15,
                left: 15,
                zIndex: 3,
              }}
            >
              <img
                width={30}
                height={30}
                src="/img/icon.png"
                style={{
                  alignSelf: 'flex-center',
                }}
              />
            </div>
            <div
              className="box-border flex flex-col justify-between text-base font-bold text-white"
              style={{
                height: 180,
                overflow: 'hidden',
                borderRadius: 5,
                backgroundColor: '#aa5d17',
                borderWidth: 1,
                borderColor: '#0d78f2',
                position: 'relative',
                top: -30,
              }}
            >
              <p style={{ marginLeft: 15, marginTop: 20 }}>Data Science</p>

              <img
                width="45%"
                src="/img/minat7.png"
                style={{
                  alignSelf: 'flex-end',
                }}
              />
            </div>
          </div>
        </div>
        <div className="grid grid-cols-1 mt-6">
          <button
            className="bg-blue-700 hover:bg-cafund-primary text-white font-bold pd-3 py-2 px-4 rounded focus:outline-none focus:shadow-outline items-center mx-auto justify-center"
            style={{ minWidth: 300 }}
            type="submit"
          >
            Pilih
          </button>
          <button
            className="text-gray-400 pd-3 py-2 px-4 rounded focus:outline-none focus:shadow-outline items-center mx-auto justify-center"
            style={{ minWidth: 100 }}
            type="submit"
          >
            Lewati
          </button>
        </div>
      </main>
    </div>
  </Layout>
);

export default minatContainer;
