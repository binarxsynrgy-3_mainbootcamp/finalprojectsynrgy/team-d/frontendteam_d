import { LENDER_CLASS_DATA } from '@/lib/constant/lender';
import { getLocalStorageValue } from '@/lib/hooks/useStorage';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import NumberFormat from 'react-number-format';

const BorrowerInformation = () => {
  const [borrower, setBorrower] = useState({});
  const router = useRouter();
  const {
    query: { name, className },
  } = router;

  useEffect(() => {
    if (typeof window !== 'undefined') {
      let newClass = LENDER_CLASS_DATA;
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        myClass.forEach((item) => {
          item.name = 'Borrower';
        });
        newClass = [...myClass, ...newClass];
      }
      const filteredBorrower = newClass.filter(
        (item) => item.className === className && item.name === name,
      );
      setBorrower(filteredBorrower[0]);
    }
  }, []);

  return (
    <div className="w-full h-screen">
      <div className="grid grid-cols-2 gap-4">
        <div className="flex flex-col gap-4">
          <div className="bg-white">
            <div className="flex flex-col p-6">
              <div
                className="mb-2 text-3xl font-semibold"
                style={{ color: '#3163AF' }}
              >
                {borrower.name}
              </div>
              <div className="mb-2 text-sm">{borrower.bootcampName}</div>
              <div className="mb-2 text-sm">{borrower.className}</div>
            </div>
          </div>
          <div className="flex flex-col bg-white p-6">
            <div className="text-xl text-gray-500">Hasil Assessment</div>
            <div className="text-3xl font-bold" style={{ color: '#3163AF' }}>
              8,5/100
            </div>
          </div>
          <div className="flex flex-col bg-white p-6">
            <div className="text-xl text-gray-500 mb-6">Riwayat Pinjaman</div>
            <div className="flex flex-row gap-5">
              <Image
                width={95}
                height={65}
                className="inline-block"
                src="/images/logo-binar.png"
                alt=""
              />
              <div>
                <div className="text-xl font-bold mt-1">
                  {borrower.bootcampName}
                </div>
                <div className="text-lg text-gray-500">
                  {borrower.className}
                </div>
                <div className="text-lg text-gray-500">Berlangsung</div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-4">
          <div className="flex flex-col px-6 py-5 bg-white">
            <div className="mb-2 text-sm">Besar Pinjaman</div>
            <div
              className="mb-2 text-3xl font-semibold"
              style={{ color: '#3163AF' }}
            >
              <NumberFormat
                value={borrower.totalAmount - borrower.dp}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
            <div className="mb-2 text-sm">
              Tenor <b style={{ color: '#3163AF' }}>{borrower.tenor} bulan</b>
            </div>
            <div className="mb-2 text-sm">
              Bunga{' '}
              <b style={{ color: '#3163AF' }}>
                {borrower.interestPercent * 100}%
              </b>
            </div>
          </div>
          <div className="flex flex-col px-6 py-5 bg-white">
            <div className="mb-2 text-sm">Prediksi Jumlah Keuntungan</div>
            <div
              className="mb-2 text-3xl font-semibold"
              style={{ color: '#3163AF' }}
            >
              <NumberFormat
                value={borrower.totalAmount - borrower.price}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
          </div>
          <div className="flex flex-col px-6 py-5 bg-white">
            <div className="mb-2 text-sm">Progres Pembayaran</div>
            <div
              className="mb-2 text-sm font-semibold"
              style={{ color: '#3163AF' }}
            >
              <b>3</b> dari 5 bulan telah dibayar
            </div>
            <div className="w-full rounded-full h-2.5">
              <div
                className="bg-cafund-primary h-2.5 rounded-full"
                style={{ width: '60%' }}
              />
            </div>
            <div className="flex flex-col gap-2 mt-2">
              <div>
                <div className="text-sm">Agustus 2021</div>
                <div
                  className="text-sm font-semibold"
                  style={{ color: '#3163AF' }}
                >
                  Rp 1.350.000
                </div>
              </div>
              <div>
                <div className="text-sm">Agustus 2021</div>
                <div
                  className="text-sm font-semibold"
                  style={{ color: '#3163AF' }}
                >
                  Rp 1.350.000
                </div>
              </div>
              <div>
                <div className="text-sm">Agustus 2021</div>
                <div
                  className="mb-2 text-sm font-semibold"
                  style={{ color: '#3163AF' }}
                >
                  Rp 1.350.000
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BorrowerInformation;
