import { useRouter } from 'next/router';
import Image from 'next/image';

export const NotifCardSuccess = () => {
  const router = useRouter();

  const handleClick = (e) => {
    e.preventDefault();
    router.push('/loan');
  };

  return (
    <div className="bg-white rounded-lg p-5">
      <div>
        <div className="text-lg font-bold text-black">
          Pinjamanmu berhasil diajukan
        </div>
        <div className="mt-21text-lg text-gray-500">
          Kelas berhasil didaftar. Pinjamanmu sedang menunggu pendana.
        </div>
        <button
          type="button"
          onClick={handleClick}
          className="mt-4 bg-cafund-primary h-10 text-white font-bold py-2 px-10 rounded-md"
        >
          Lihat Sekarang
        </button>
      </div>
    </div>
  );
};

export const NotifCardFunded = () => {
  const router = useRouter();

  const handleClick = (e) => {
    e.preventDefault();
    router.push('/loan');
  };

  return (
    <div className="bg-white rounded-lg p-5">
      <div>
        <div className="text-lg font-bold text-black">Pinjamanmu didanai!</div>
        <div className="mt-21text-lg text-gray-500">
          Pinjaman yang kamu ajukan telah didanai oleh pendana. Waktunya
          menandatangani perjanjian!
        </div>
        <button
          type="button"
          onClick={handleClick}
          className="mt-4 bg-cafund-primary h-10 text-white font-bold py-2 px-10 rounded-md"
        >
          Lihat Sekarang
        </button>
      </div>
    </div>
  );
};

export const NotifCardPaymentDue = () => {
  const router = useRouter();

  const handleClick = (e) => {
    e.preventDefault();
    router.push('/loan');
  };

  return (
    <div className="bg-white rounded-lg p-5">
      <div>
        <div className="text-lg font-bold text-black">
          Pembayaranmu hampir jatuh tempo
        </div>
        <div className="mt-21text-lg text-gray-500">
          3 hari lagi pembayaranmu jatuh tempo. Segera lakukan pembayaran.
        </div>
        <button
          type="button"
          onClick={handleClick}
          className="mt-4 bg-cafund-primary h-10 text-white font-bold py-2 px-10 rounded-md"
        >
          Lihat Sekarang
        </button>
      </div>
    </div>
  );
};

export const NoNotif = () => (
  <div className="flex text-center overscroll-none">
    <div className="m-auto mt-12 w-3/5">
      <Image
        width={225}
        height={191}
        className="mx-40"
        src="/images/NoNotif.png"
        alt=""
      />
      <div className="mt-14 text-xl font-bold text-black">
        Yah, notifikasi kamu kosong
      </div>
      <div className="mb-8 text-lg text-gray-500">
        Kamu belum memiliki notifikasi apapun, lakukan peminjaman melalui
        smartphone kamu untuk mendapatkan informasi terbaru.
      </div>
      <button>
        <Image
          width={204}
          height={63}
          className="inline-block py-auto"
          src="/images/btn-download.png"
          alt=""
        />
      </button>
    </div>
  </div>
);

const Notification = () => (
  <div className="h-screen box-border flex flex-col gap-5">
    <h1 className="text-4xl mb-4  font-bold text-cafund-primary">Notifikasi</h1>
    {/* <NotifCardSuccess />
    <NotifCardFunded />
    <NotifCardPaymentDue /> */}
    <NoNotif />
  </div>
);

export default Notification;
