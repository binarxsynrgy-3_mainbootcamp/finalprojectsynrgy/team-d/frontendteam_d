import { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import useAssementDispatch from '@/redux/assesment/dispatch';
import { getLocalStorageValue, useGetStorage } from '@/lib/hooks/useStorage';
import * as moment from 'moment';

const WelcomeBox = () => {
  const router = useRouter();
  return (
    <div className="bg-white w-full p-8 gap-2 flex flex-col mt-8">
      <div className="font-bold text-2xl">Hi, Borrower</div>
      <div className="text-2xl">Ayo lakukan assesment!</div>
      <div className="mt-8">
        <button
          type="button"
          className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md"
          onClick={() => router.push('/assessment')}
        >
          Lakukan Assessment
        </button>
      </div>
    </div>
  );
};

const MainBox = () => {
  const { assesment } = useAssementDispatch();
  const [myClasses, setMyClasses] = useState([]);
  useEffect(() => {
    if (typeof window !== 'undefined') {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        setMyClasses(myClass);
      }
    }
  }, []);

  return (
    <div>
      <div className="bg-white w-full p-8 gap-4 flex flex-col mt-8">
        <div className="text-2xl">Nilai Asesmen</div>
        <div className="font-bold text-5xl text-cafund-primary">
          {assesment.testScores}
        </div>
        {assesment && assesment.isDone && assesment.testScores <= 85 && (
          <>
            <div>
              <button
                type="button"
                className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md disabled:bg-gray-400"
                disabled
              >
                Ulangi Assessment
              </button>
            </div>
            <div className="text-lg text-gray-500">
              Bisa dilakukan mulai{' '}
              {moment(assesment.workDate).add(2, 'd').format('DD MMMM YYYY')}
            </div>
          </>
        )}
      </div>
      <h1 className="text-xl font-bold text-cafund-primary my-8">
        Kelas yang sudah diambil
      </h1>
      {myClasses && myClasses.length > 0 ? (
        <div className="flex flex-col gap-4">
          {myClasses.map((classItem) => (
            <div className="bg-white w-full p-8 gap-2 flex flex-row justify-between">
              <div className="flex flex-row gap-3">
                <Image
                  width={50}
                  height={50}
                  className="inline-block"
                  src={classItem.logo}
                  alt=""
                />
                <div>
                  <div className="text-xl font-bold">
                    {classItem.bootcampName}
                  </div>
                  <div className="text-lg ">{classItem.className}</div>
                </div>
              </div>
              <div className="flex flex-row gap-10">
                <div>
                  <div className="text-lg text-gray-500">Tanggal Bootcamp</div>
                  <div className="text-xl font-bold text-cafund-primary">
                    April - Juni 2021
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      ) : (
        <div className="text-cafund-primary bg-white h-40 flex items-center justify-center">
          Belum ada kelas yang di ambil :(
        </div>
      )}
    </div>
  );
};

const DashboardBorrower = () => {
  const assesmentStorage = useGetStorage('assesment', false);
  const { assesment, defaultAssesment, setAssement } = useAssementDispatch();

  useEffect(() => {
    if (!assesmentStorage) {
      defaultAssesment();
    } else {
      setAssement(assesmentStorage);
    }
  }, [assesmentStorage, defaultAssesment]);

  return (
    <div className="dashboard-borrower min-h-screen h-full mb-16">
      <h1 className="text-4xl font-bold text-cafund-primary">Dashboard</h1>
      {assesment && assesment.isDone ? <MainBox /> : <WelcomeBox />}
    </div>
  );
};

export default DashboardBorrower;
