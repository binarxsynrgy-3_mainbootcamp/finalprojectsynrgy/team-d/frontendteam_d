import { CLASS_LIST } from '@/lib/constant/class';
import { alertError } from '@/lib/hooks/toast';
import useModalDispatch from '@/redux/modal/dispatch';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';
import NumberFormat from 'react-number-format';
import InstallmentModal from './installment-modal';

const InstallmentContainer = () => {
  const [classDetail, setClassDetail] = useState({});
  const [bankInterest, setBankInterest] = useState(0);
  const [interestPercent, setInterestPercent] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  const [errorDP, setErrorDP] = useState(null);
  const [errorTenor, setErrorTenor] = useState(null);
  const [disableSubmit, setDisableSubmit] = useState(true);
  const [inputValues, setInputValues] = React.useState({
    dp: '',
    tenor: '',
  });
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const {
    query: { id, className },
  } = router;

  useEffect(() => {
    const filteredData = CLASS_LIST.filter(
      (item) => item.id === id && item.className === className,
    )[0];
    if (filteredData) {
      const amountInterest = Math.ceil(filteredData.price * interestPercent);
      setBankInterest(amountInterest);
      setTotalAmount(amountInterest + filteredData.price);
      setClassDetail({
        ...filteredData,
        amountInterest,
        interestPercent,
        totalAmount: amountInterest + filteredData.price,
      });
    }
  }, [interestPercent]);

  const handleSubmit = () => {
    setClassDetail({
      ...classDetail,
      dp: inputValues.dp,
      tenor: inputValues.tenor,
    });
    setIsOpen(true);
  };

  const handleChange = (e) => {
    const { value } = e.target;
    if (e.target.name === 'dp') {
      const minDP = Math.ceil((classDetail.price * 10) / 100);
      const maxDP = Math.ceil((classDetail.price * 30) / 100);
      if (+value < minDP || +value > maxDP) {
        setErrorDP(`Nominal Rp${minDP} - Rp${maxDP}`);
      } else {
        setErrorDP('');
      }
    }

    if (e.target.name === 'tenor') {
      if (+value < 3 || +value > 12) {
        setErrorTenor('Minimal 3 bulan dan maksimal 12 bulan');
        setInterestPercent(0);
      } else {
        const percent = ((+value / 12) * 0.05).toFixed(3);
        setInterestPercent(percent);
        setErrorTenor('');
      }
    }
    setInputValues({
      ...inputValues,
      [e.target.name]: value,
    });
  };

  useEffect(() => {
    if (errorDP === '' && errorTenor === '') {
      setDisableSubmit(false);
    } else {
      setDisableSubmit(true);
    }
  }, [errorDP, errorTenor]);

  return (
    <div className="h-screen">
      <div>
        <h1 className="text-4xl font-bold text-cafund-primary">Atur Cicilan</h1>
        <div className="flex items-center flex-row justify-between w-full mt-11">
          <div className="flex flex-row gap-5">
            <Image
              width={65}
              height={65}
              className="inline-block"
              src="/images/logo-binar.png"
              alt=""
            />
            <div>
              <div className="text-xl font-bold mt-1">
                {classDetail?.bootcampName}
              </div>
              <div className="text-lg ">{classDetail?.className}</div>
            </div>
          </div>
          <div className="flex flex-row gap-8">
            <div>
              <div className="text-lg text-gray-500">Harga Bootcamp</div>
              <div className="text-xl font-bold text-cafund-primary">
                <NumberFormat
                  value={classDetail?.price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>
            </div>
            <div>
              <div className="text-lg text-gray-500">Durasi Bootcamp</div>
              <div className="text-xl font-bold text-cafund-primary">
                {classDetail?.duration}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="block lg:flex justify-between gap-x-20 xl:gap-x-56 bg-white mt-7 rounded-lg px-16 py-14">
          <form className="flex-grow flex flex-col gap-4">
            <label htmlFor="dp">
              Uang Muka (DP)
              <input
                className="block w-full px-4 mt-2 border-2 border-gray-100  bg-gray-100 h-10 rounded-lg focus:outline-none "
                type="number"
                id="dp"
                name="dp"
                placeholder="Masukkan Nominal DP"
                value={inputValues.dp}
                onChange={handleChange}
              />
              {(errorDP !== '' || errorDP !== null) && (
                <p className="text-red-500">{errorDP}</p>
              )}
            </label>
            <label htmlFor="tenor">
              Durasi Cicilan
              <input
                className="block w-full px-4 mt-2 border-2 border-gray-100  bg-gray-100 h-10 rounded-lg focus:outline-none "
                type="number"
                id="tenor"
                name="tenor"
                placeholder="Masukkan Durasi Cicilan (Dalam Bulan)"
                value={inputValues.tenor}
                onChange={handleChange}
              />
              {(errorTenor !== '' || errorTenor !== null) && (
                <p className="text-red-500">{errorTenor}</p>
              )}
            </label>
          </form>
          <div className="flex-none">
            <div>
              <div className="text-lg text-gray-500">Bunga</div>
              <div className="text-xl font-bold text-cafund-primary">
                {interestPercent * 100}%
              </div>
            </div>
            <div className="mt-4">
              <div className="text-lg text-gray-500">Nominal Bunga</div>
              <div className="text-xl font-bold text-cafund-primary">
                <NumberFormat
                  value={bankInterest}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>
            </div>
            <div className="mt-4">
              <div className="text-lg text-gray-500">
                Total Pembayaran (Harga+Bunga)
              </div>
              <div className="text-xl font-bold text-cafund-primary">
                <NumberFormat
                  value={totalAmount}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="flex justify-center gap-4 mt-4">
          <button
            type="button"
            className="bg-white font-bold py-2 px-4 rounded-md w-40"
            onClick={() => router.back()}
          >
            Kembali
          </button>
          <button
            type="button"
            className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
            onClick={handleSubmit}
            disabled={disableSubmit}
          >
            Selesai
          </button>
        </div>
      </div>

      {isOpen && (
        <InstallmentModal
          classDetail={classDetail}
          isOpen={isOpen}
          closeModal={() => setIsOpen(false)}
        />
      )}
    </div>
  );
};

export default InstallmentContainer;
