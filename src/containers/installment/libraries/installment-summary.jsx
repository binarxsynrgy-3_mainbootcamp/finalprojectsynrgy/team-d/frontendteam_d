import NumberFormat from 'react-number-format';
import Image from 'next/image';
import { useState } from 'react';

const InstallmentSummary = ({ classDetail, handleSubmit, handleClose }) => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <div className="flex flex-col gap-4 w-full">
      <div className="bg-white rounded-lg">
        <div className="flex items-center flex-row justify-between w-full  p-8">
          <div className="flex flex-row gap-5">
            <Image
              width={65}
              height={65}
              className="inline-block"
              src="/images/logo-binar.png"
              alt=""
            />
            <div>
              <div className="text-xl font-bold mt-1">
                {classDetail.bootcampName}
              </div>
              <div className="text-lg ">{classDetail.className}</div>
            </div>
          </div>
          <div className="flex flex-row gap-8">
            <div>
              <div className="text-lg text-gray-500">Durasi Bootcamp</div>
              <div className="text-xl font-bold text-cafund-primary">
                {classDetail.duration}
              </div>
            </div>
            <div>
              <div className="text-lg text-gray-500">Harga Bootcamp</div>
              <div className="text-xl font-bold text-cafund-primary">
                <NumberFormat
                  value={classDetail.price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-white rounded-lg">
        <div className="pl-6 pt-4 text-xl text-gray-500">Informasi Cicilan</div>
        <div className="flex flex-row gap-6 w-full p-8">
          <div>
            <div className="text-lg text-gray-500">Durasi Cicilan</div>
            <div className="text-xl font-bold text-cafund-primary">
              {classDetail.tenor} bulan
            </div>
          </div>
          <div>
            <div className="text-lg text-gray-500">Bunga</div>
            <div className="text-xl font-bold text-cafund-primary">
              {classDetail.interestPercent * 100}%
            </div>
          </div>
          <div>
            <div className="text-lg text-gray-500">Nominal Bunga</div>
            <div className="text-xl font-bold text-cafund-primary">
              <NumberFormat
                value={classDetail.amountInterest}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
          </div>
          <div>
            <div className="text-lg text-gray-500">Uang Muka (DP)</div>
            <div className="text-xl font-bold text-cafund-primary">
              <NumberFormat
                value={classDetail.dp}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
          </div>
          <div>
            <div className="text-lg text-gray-500">Total Pembayaran</div>
            <div className="text-xl font-bold text-cafund-primary">
              <NumberFormat
                value={classDetail.totalAmount}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-end gap-2 p-4 border-blueGray-200 rounded-b">
        <button
          className="bg-white font-bold py-2 px-4 rounded-md w-40"
          type="button"
          onClick={handleClose}
        >
          Close
        </button>
        <button
          className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
          type="button"
          onClick={() => {
            setIsLoading(true);
            setTimeout(() => {
              setIsLoading(false);
              handleSubmit();
            }, 3000);
          }}
          disabled={isLoading}
        >
          {isLoading ? 'Submitting...' : 'Submit'}
        </button>
      </div>
    </div>
  );
};

export default InstallmentSummary;
