import ModalCustom from '@/components/ModalCustom';
import { alertSuccess } from '@/lib/hooks/toast';
import {
  getLocalStorageValue,
  setLocalStorageValue,
} from '@/lib/hooks/useStorage';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { useRouter } from 'next/router';
import { useState } from 'react';
import InstallmentPay from './installment-pay';
import InstallmentSummary from './installment-summary';

const InstallmentModal = ({ classDetail, isOpen, closeModal }) => {
  const router = useRouter();
  const { doShowSpinner, doHideSpinner } = useSpinnerDispatch();
  const [openPaymentMethod, setOpenPaymentMethod] = useState(false);

  const handleCloseModal = () => {
    if (openPaymentMethod) {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        const newClass = [...myClass, classDetail];
        setLocalStorageValue('myClass', newClass);
      } else {
        setLocalStorageValue('myClass', [classDetail]);
      }
      doShowSpinner();
      alertSuccess(
        'Data kelas telah di tambahkan ke halaman pinjaman, klik menu pinjaman untuk melihat',
      );
      setTimeout(() => {
        doHideSpinner();
        closeModal();
        router.back();
      }, 3000);
    } else {
      closeModal();
    }
  };

  return (
    <ModalCustom
      isOpen={isOpen}
      title={'Ringkasan'}
      closeModal={handleCloseModal}
    >
      {openPaymentMethod ? (
        <InstallmentPay
          classDetail={classDetail}
          handleCloseModal={handleCloseModal}
        />
      ) : (
        <InstallmentSummary
          classDetail={classDetail}
          handleSubmit={() => setOpenPaymentMethod(true)}
          handleClose={closeModal}
        />
      )}
    </ModalCustom>
  );
};

export default InstallmentModal;
