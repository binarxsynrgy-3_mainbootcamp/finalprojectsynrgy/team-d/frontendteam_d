import { Box, Button, Image, Link } from '@chakra-ui/react';
import { Formik } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';

import useSpinnerDispatch from '@/redux/spinner/dispatch';
import AuthRepository from '@/lib/repositories/auth';
import { alertError, alertSuccess } from '@/lib/hooks/toast';
import useAuthDispatch from '@/redux/auth/dispatch';
import { useGetStorage } from '@/lib/hooks/useStorage';
import { useEffect } from 'react';

const validationSchema = Yup.object({
  email: Yup.string()
    .email('format email tidak sesuai')
    .required('email harus diisi'),
  password: Yup.string()
    .required('password harus diisi')
    .min(8, 'password minimal 8 karakter'),
});

const initialValues = {
  username: '',
  password: '',
};

const LoginContainer = () => {
  const router = useRouter();
  const value = useGetStorage('authentication', false);
  const { doLogin } = useAuthDispatch();
  const { doHideSpinner, doShowSpinner, showSpinner } = useSpinnerDispatch();

  useEffect(() => {
    if (value) {
      router.push('/');
    }
  }, [value]);

  const handleOnSubmit = async (values) => {
    const { email, password } = values;
    doShowSpinner(true);
    const payload = {
      email,
      password,
    };
    const data = await AuthRepository().signin(payload);
    if (data) {
      if (data.code === 307) {
        alertSuccess('Oops, sepertinya anda perlu melakukan verifikasi OTP');
      } else if (data.code === 401) {
        alertError(data.message);
      } else {
        alertSuccess('Berhasil masuk, mengarahkan ke halaman dashboard');
      }
      setTimeout(() => {
        doHideSpinner(false);
        if (data.code === 307) {
          router.push({
            pathname: '/otp',
            query: { email },
          });
        } else if (!data.code) {
          localStorage.setItem('authentication', JSON.stringify(data));
          doLogin(data);
          router.push('/');
        }
      }, 3000);
    }
  };

  return (
    <div className="h-screen">
      <Box className="flex mx-auto justify-between my-24 px-16">
        <Box className="place-content-center w-96">
          <h1 className="mb-16 text-4xl text-center font-bold">Masuk</h1>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({ handleSubmit, handleChange, handleBlur, touched, errors }) => (
              <form
                className="form__container space-y-2"
                onSubmit={handleSubmit}
              >
                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="email">
                    <input
                      name="email"
                      id="email"
                      placeholder="Email"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.email && (
                      <span
                        className={`text-red-500 form__control-message ${
                          touched.email && errors.email ? 'error' : ''
                        }`}
                      >
                        {touched.email && errors.email && errors.email}
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="password">
                    <input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="Password"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.password && (
                      <span
                        className={`form__control-message ${
                          touched.password && errors.password
                            ? 'text-red-500'
                            : ''
                        }`}
                      >
                        {touched.password && errors.password && errors.password}
                      </span>
                    )}
                  </div>
                </div>
                <div className="form__group form__actions">
                  <div>
                    <Button
                      w="100%"
                      colorScheme="cafund"
                      px={8}
                      type="submit"
                      className="mt-16"
                      disabled={showSpinner}
                    >
                      {showSpinner ? 'Mohon tunggu...' : 'Masuk'}
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
          <Box className="mt-8">
            <p className="mt-5 text-center">
              Belum punya akun?
              <Link
                href="/register"
                color="blue.500"
                fontWeight="bold"
                marginLeft="1"
              >
                Daftar
              </Link>
            </p>
          </Box>
        </Box>

        <div className="flex flex-col items-center justify-center">
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="100%"
              h="100%"
              src="/images/login.png"
            />
          </Box>
        </div>
      </Box>
    </div>
  );
};

export default LoginContainer;
