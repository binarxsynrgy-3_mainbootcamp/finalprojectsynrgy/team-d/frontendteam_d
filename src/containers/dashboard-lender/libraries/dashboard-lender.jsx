import { LENDER_CLASS_DATA } from '@/lib/constant/lender';
import { getLocalStorageValue } from '@/lib/hooks/useStorage';
import { useEffect, useState } from 'react';
import Image from 'next/image';
import NumberFormat from 'react-number-format';
import DashboardLenderModal from './dashboard-lender-modal';

export const NoBorrower = () => {
  return (
    <div className="flex text-center overscroll-none">
      <div className="m-auto mt-12 w-3/6">
        <Image
          width={225}
          height={225}
          className="mx-40"
          src="/images/class-not-found.png"
          alt=""
        />
        <div className="mt-14 text-xl font-bold text-black">
          Yah, kamu belum pernah melakukan pendanaan
        </div>
        <div className="mb-8 text-lg text-gray-500">
          Gunakan CareerFund di smartphone kamu untuk melakukan pendanaan dan
          dapatkan
          <span className="text-cafund-primary font-bold">
            cashback s.d. 10%
          </span>
          untuk transaksi yang kamu lakukan.
        </div>
        <button>
          <Image
            width={204}
            height={63}
            className="inline-block py-auto"
            src="/images/btn-download.png"
            alt=""
          />
        </button>
      </div>
    </div>
  );
};

const DashboardLender = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [myClasses, setMyClasses] = useState(LENDER_CLASS_DATA);
  const [selectedBorrower, setSelectedBorrower] = useState({});

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        myClass.forEach((item) => {
          item.name = 'Borrower';
        });
        const newClass = [...myClass, ...myClasses];
        if (newClass.length !== myClass.length) {
          setMyClasses(newClass);
        }
      }
    }
  }, []);

  const loadBorrower = () => {
    const myClass = getLocalStorageValue('myClass');
    if (myClass) {
      myClass.forEach((item) => {
        item.name = 'Borrower';
      });
      const newClass = [...myClass, ...LENDER_CLASS_DATA];
      setMyClasses(newClass);
    }
  };

  const totalAssets = () => {
    const filtered = myClasses.filter((item) => item.isFunded);
    const total = filtered.reduce((acc, cur) => {
      return acc + cur.price;
    }, 0);
    return total;
  };

  const openDetail = (index) => {
    setSelectedBorrower(myClasses[index]);
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div
      className="w-full min-h-screen
     h-full"
      style={{ backgroundColor: '#F2F2F7' }}
    >
      {myClasses.length > 0 ? (
        <>
          <div className="w-full" style={{ backgroundColor: '#F2F2F7' }}>
            <div
              className="pt-5 flex  mx-auto justify-center"
              style={{ backgroundColor: '#F2F2F7' }}
            >
              <div className="flex-none" />
              <p className=" text-2xl text-left text-cafund-primary md:text-cafund-primary font-bold">
                Dashboard
              </p>
              <div className="flex-grow h-16 ..." />
              <div className="grid grid-cols-1 md:grid-cols-1">
                <div>
                  <p className="text-xs text-grey text-left">Total Aset</p>
                </div>
                <div>
                  <p
                    className="mb-8 text-1xl text-right text-cafund-primary md:text-cafund-primary font-bold"
                    style={{ textAlign: 'right' }}
                  >
                    <NumberFormat
                      value={totalAssets()}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'Rp'}
                    />
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-col mx-auto justify-center">
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table className="min-w-full md:w-full lg:w-full divide-y divide-gray-200">
                    <tbody className="bg-white divide-y divide-gray-200">
                      {myClasses.map((borrower, index) => (
                        <tr key={index}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="ml-4">
                                <div className="text-left font-bold">
                                  {borrower.name}
                                </div>
                                <div
                                  className="text-left font-bold"
                                  style={{ color: '#3163AF' }}
                                >
                                  <NumberFormat
                                    value={borrower.totalAmount - +borrower.dp}
                                    displayType={'text'}
                                    thousandSeparator={true}
                                    prefix={'Rp'}
                                  />
                                </div>
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="text-sm">Lama Cicilan</div>
                            <div
                              className="text-left font-bold"
                              style={{ color: '#3163AF' }}
                            >
                              {borrower.tenor}
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="text-sm">Bunga</div>
                            <div
                              className="text-left font-bold"
                              style={{ color: '#3163AF' }}
                            >
                              {borrower.interestPercent * 100}%
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap ">
                            {borrower.isFunded ? (
                              <>
                                <div className="text-sm">Progres Pinjaman</div>
                                <div className="w-full bg-gray-200 rounded-full h-2.5">
                                  <div
                                    className="bg-cafund-primary h-2.5 rounded-full"
                                    style={{
                                      width:
                                        borrower.name === 'Borrower' ? 0 : 45,
                                    }}
                                  />
                                </div>
                              </>
                            ) : (
                              <>
                                <div className="text-sm">Status Pinjaman</div>
                                <div className="text-sm text-cafund-primary font-bold">
                                  Menunggu Pendanaan
                                </div>
                              </>
                            )}
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <button
                              data-bs-dismiss="modal"
                              variant="primary"
                              onClick={() => openDetail(index)}
                              className="modal-title bg-cafund-primary hover:bg-cafund-primary text-white text-center rounded focus:outline-none focus:shadow-outline"
                              id="exampleModalLabel"
                              style={{
                                minWidth: 150,
                                minHeight: 30,
                                borderRadius: 7,
                              }}
                            >
                              Detail
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        NoBorrower()
      )}

      <DashboardLenderModal
        isOpen={isOpen}
        closeModal={() => {
          loadBorrower();
          closeModal();
        }}
        borrower={selectedBorrower}
      />
    </div>
  );
};

export default DashboardLender;
