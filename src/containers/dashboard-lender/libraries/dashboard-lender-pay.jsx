import { useState } from 'react';
import { BANK } from '@/lib/constant/class';
import Image from 'next/image';
import NumberFormat from 'react-number-format';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import {
  getLocalStorageValue,
  setLocalStorageValue,
} from '@/lib/hooks/useStorage';
import { alertSuccess } from '@/lib/hooks/toast';

const DashboardLenderPay = ({ classDetail, closeModal }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isMobileBanking, setIsMobileBanking] = useState(false);
  const [isDetailPayment, setIsDetailPayment] = useState(true);
  const [selectedBank, setSelectedBank] = useState(null);
  const { doShowSpinner, doHideSpinner } = useSpinnerDispatch();
  const [bank, setBank] = useState(BANK);

  const onChangeBank = (id) => {
    const newbBank = bank.map((item) => {
      if (item.id === id) {
        item.checked = !item.checked;
      } else {
        item.checked = false;
      }
      return item;
    });

    const checkedBank = bank.find((item) => item.checked);
    checkedBank ? setSelectedBank(checkedBank) : setSelectedBank();
    setBank(newbBank);
  };

  const handlePay = () => {
    setIsLoading(true);
    doShowSpinner();
    setTimeout(() => {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        myClass.forEach((item) => {
          if (
            item.id === classDetail.id &&
            item.className === classDetail.className
          ) {
            item.isFunded = true;
          }
        });
      }
      alertSuccess('Berhasil melakukan pembayaran');
      setLocalStorageValue('myClass', myClass);
      doHideSpinner();
      setIsLoading(false);
      closeModal();
    }, 3000);
  };

  return (
    <>
      {isDetailPayment ? (
        <div>
          <div
            className="mt-4 flex flex-col gap-5"
            style={{
              height: '600px',
              overflowY: 'scroll',
            }}
          >
            <div className="p-3 w-full bg-white rounded-lg">
              <div className="text-md text-gray-500">Pembayaran Penuh</div>
              <div className="text-3xl font-bold text-cafund-primary">
                <NumberFormat
                  value={+classDetail.totalAmount - +classDetail.dp}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>

              {selectedBank ? (
                <div className="mt-4">
                  <div className="text-md text-gray-500">
                    Nomor Virtual Account (VA)
                  </div>
                  <div className="text-3xl font-bold text-cafund-primary">
                    32842058306923
                  </div>
                  <div className="flex justify-start gap-24 mt-4">
                    <div>
                      <div className="text-md text-gray-500">Bank</div>
                      <div
                        style={{
                          width: '240%',
                          height: '0%',
                          position: 'relative',
                        }}
                      >
                        <Image
                          width="100%"
                          height="40%"
                          layout="responsive"
                          objectFit="contain"
                          src={selectedBank.logoTransparant}
                          alt=""
                        />
                      </div>
                    </div>
                    <div>
                      <div className="text-md text-gray-500">Bayar Sebelum</div>
                      <div className="text-md mt-1 font-bold text-cafund-primary">
                        13 November 2021
                      </div>
                    </div>
                  </div>
                  <button
                    type="button"
                    onClick={() => setIsDetailPayment(false)}
                    className="mt-10 bg-cafund-primary h-10 text-white font-bold py-2 px-4 rounded-md"
                  >
                    Ganti Bank
                  </button>
                </div>
              ) : (
                <div className="flex justify-start mt-4">
                  <div className="text-md text-gray-500">Bayar Sebelum</div>
                  <div className="text-md text-cafund-primary">
                    13 November 2021
                  </div>
                </div>
              )}
            </div>

            {selectedBank ? (
              ''
            ) : (
              <div className="p-3 w-full bg-white rounded-lg">
                <div className="mb-4 text-md text-gray-500">
                  Metode Pembayaran
                </div>
                <button
                  type="button"
                  onClick={() => setIsDetailPayment(false)}
                  className="bg-cafund-primary h-10 text-white font-bold py-2 px-4 rounded-md"
                >
                  Pilih Metode Pembayaran
                </button>
              </div>
            )}

            <div className="p-3 w-full bg-white rounded-lg">
              <div className="text-md text-gray-500">Cara Pembayaran</div>
              <div className="flex my-4">
                <button
                  type="button"
                  onClick={() => setIsMobileBanking(false)}
                  className={
                    isMobileBanking
                      ? 'mr-8 grow border-2 border-cafund-primary bg-white h-10 text-cafund-primary font-bold py-auto px-4 rounded-md'
                      : 'mr-8 grow bg-cafund-primary h-10 text-white font-bold py-2 px-4 rounded-md'
                  }
                >
                  Melalui Transfer ATM
                </button>
                <button
                  type="button"
                  onClick={() => setIsMobileBanking(true)}
                  className={
                    isMobileBanking
                      ? 'grow bg-cafund-primary h-10 text-white font-bold py-2 px-4 rounded-md'
                      : 'grow border-2 border-cafund-primary bg-white h-10 text-cafund-primary font-bold py-auto px-4 rounded-md'
                  }
                >
                  Melalui Mobile Banking
                </button>
              </div>
              <ol className="text-md list-decimal ml-4 text-black">
                {isMobileBanking ? (
                  <div>
                    <li>Silahkan login ke mobile banking Anda.</li>
                    <li>Klik “Icon Menu” di sebelah kiri atas. </li>
                    <li>Pilih menu “Pembayaran”</li>
                    <li> Lanjut buat “Pembayaran Baru” </li>
                    <li> Pilih “Multi Payment”</li>
                    <li>
                      Klik “Penyedia Jasa” atau “Service Provider”, kemudian
                      pilih kode perusahaan
                    </li>
                    <li> Pilih “No. Virtual” </li>
                    <li>
                      Masukkan nomor Virtual Account Anda dengan kode perusahaan
                      (contoh 8890802001287578), kemudian pilih “Tambah Sebagai
                      Nomor Baru”
                    </li>
                    <li>
                      Masukkan nominal lalu pilih “Konfirmasi” dan “lanjut”
                    </li>
                    <li>
                      Selanjutnya akan muncul tampilan konfirmasi pembayaran.
                      Pastikan semua informasi dan total tagihan sudah benar.
                      Jika sudah benar, lalu scroll ke bawah dan pilih
                      “Konfirmasi”
                    </li>
                    <li>Masukkan PIN Anda dan pilih “OK” </li>
                    <li> Transaksi Anda telah selesai</li>
                  </div>
                ) : (
                  <div>
                    <li className="text-md list-decimal text-black">
                      Pilih menu “Bayar/Beli
                    </li>
                    <li>
                      Selanjutnya akan muncul tipe pembayaran seperti Multi
                      Payment dan Bill Payment (PLN, PDAM, dll), untuk
                      pembayaran VA pilih “Multi Payment”
                    </li>
                    <li>
                      Silahkan masukkan kode perusahaan, contoh: “88908”
                      (Xendit), lalu pilih “Benar”
                    </li>
                    <li>
                      Masukkan nomor Virtual Account Anda (contoh:
                      8890802001287578) dan klik “Benar”
                    </li>
                    <li>
                      Isikan nominal yang akan dibayarkan, kemudian tekan
                      “Benar”
                    </li>
                    <li>
                      Jangan lupa untuk memeriksa informasi yang tertera pada
                      layar. Pastikan semua informasi dan total tagihan yang
                      ditampilkan sudah benar. Jika benar, tekan angka 1 dan
                      pilih “Ya”
                    </li>
                    <li>
                      Periksa layar konfirmasi dan pilih “Ya” untuk melakukan
                      pembayaran
                    </li>
                    <li>
                      Bukti pembayaran dalam bentuk struk agar disimpan sebagai
                      bukti pembayaran yang sah dari Bank BCA
                    </li>
                    <li>Transaksi Anda telah selesai'</li>
                  </div>
                )}
              </ol>
            </div>
          </div>
          <div className="flex items-center justify-end pt-4 gap-2">
            <button
              className="bg-white font-bold py-2 px-4 rounded-md w-40"
              type="button"
              onClick={closeModal}
            >
              Close
            </button>
            <button
              className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
              type="button"
              onClick={() => {
                handlePay();
              }}
              disabled={isLoading || !selectedBank}
            >
              Bayar (Testing)
            </button>
          </div>
        </div>
      ) : (
        <div className="mx-10 my-8">
          <div className="text-lg font-bold"> Virtual Account</div>
          {BANK.map((bankItem) => (
            <div key={bankItem.id} className="w-full mt-4">
              <label className="flex justify-between items-start gap-3 px-5 p-3 w-full bg-white rounded-lg">
                <div className="flex gap-5 justify-start select-none">
                  <Image
                    width={56}
                    height={56}
                    className="inline-block py-auto"
                    src={bankItem.logo}
                    alt=""
                  />

                  <div className="my-auto text-lg text-gray-500">
                    Bank {bankItem.names}
                  </div>
                </div>
                <div className="w-6 h-6 flex flex-shrink-0 justify-center items-center my-auto focus-within:border-cafund-primary">
                  <input
                    type="checkbox"
                    className="block opacity-0 absolute"
                    value={bankItem.id}
                    checked={bankItem.checked}
                    onChange={() => onChangeBank(bankItem.id)}
                  />
                  <svg
                    className={
                      (bankItem.checked ? 'block' : 'hidden') +
                      ' fill-current w-32 h-32 text-cafund-primary pointer-events-none'
                    }
                    viewBox="1 1 18 18"
                  >
                    <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                  </svg>
                </div>
              </label>
            </div>
          ))}

          <div className="flex justify-center gap-4 my-10">
            <button
              type="button"
              onClick={() => setIsDetailPayment(true)}
              className="bg-cafund-primary h-10 text-white font-bold py-2 w-full rounded-md"
            >
              Pilih
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default DashboardLenderPay;
