import ModalCustom from '@/components/ModalCustom';
import { alertSuccess } from '@/lib/hooks/toast';
import {
  getLocalStorageValue,
  setLocalStorageValue,
} from '@/lib/hooks/useStorage';
import { useRouter } from 'next/router';
import { useState } from 'react';
import NumberFormat from 'react-number-format';
import DashboardLenderPay from './dashboard-lender-pay';

const DashboardLenderModal = ({ isOpen, closeModal, borrower }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [openPayment, setOpenPayment] = useState(false);
  const router = useRouter();

  const openProfile = (name, className) => {
    router.push({
      pathname: '/borrower-information',
      query: {
        name,
        className,
      },
    });
  };

  return (
    <ModalCustom
      closeModal={closeModal}
      title="Bantu Pendanaan"
      isOpen={isOpen}
    >
      {openPayment ? (
        <DashboardLenderPay classDetail={borrower} closeModal={closeModal} />
      ) : (
        <div className="flex justify-center w-full items-center antialiased">
          <div className="flex flex-col w-full gap-4">
            <div className="flex flex-col px-6 py-5 bg-white">
              <div className="mb-2 text-sm">Besar Pinjaman</div>
              <div
                className="mb-2 text-3xl font-semibold"
                style={{ color: '#3163AF' }}
              >
                <NumberFormat
                  value={borrower.totalAmount - +borrower.dp}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </div>
              <div className="mb-2 text-sm">
                Tenor <b style={{ color: '#3163AF' }}>{borrower.tenor} Bulan</b>
              </div>
              <div className="mb-2 text-sm">
                Bunga{' '}
                <b style={{ color: '#3163AF' }}>
                  {borrower.interestPercent * 100}%
                </b>
              </div>
            </div>

            {borrower.isFunded && (
              <div className="flex flex-col px-6 py-5 bg-white">
                <div className="mb-2 text-sm">Progres Pembayaran</div>
                <div
                  className="mb-2 text-sm font-semibold"
                  style={{ color: '#3163AF' }}
                >
                  <b>3</b> dari {borrower.tenor} bulan telah dibayar
                </div>
                <div className="w-full rounded-full h-2.5">
                  <div
                    className="bg-cafund-primary h-2.5 rounded-full"
                    style={{ width: '60%' }}
                  />
                </div>
              </div>
            )}

            <div className="bg-white">
              <div className="flex flex-col px-6 py-5">
                <div
                  className="mb-2 text-3xl font-semibold"
                  style={{ color: '#3163AF' }}
                >
                  {borrower.name}
                </div>
                <div className="mb-2 text-sm">{borrower.bootcampName}</div>
                <div className="mb-2 text-sm">{borrower.className}</div>
              </div>
              <div className="flex flex-row items-center justify-between p-5 rounded-bl-lg rounded-br-lg">
                <button
                  className="text-white bg-cafund-primary rounded"
                  style={{
                    minWidth: 200,
                    minHeight: 30,
                    borderRadius: 7,
                  }}
                  onClick={() => {
                    openProfile(borrower.name, borrower.className);
                  }}
                >
                  Informasi Peminjaman
                </button>
              </div>
            </div>

            {!borrower.isFunded && (
              <div className="flex items-center justify-end gap-2 p-4 border-blueGray-200 rounded-b">
                <button
                  className="bg-white font-bold py-2 px-4 rounded-md w-full"
                  type="button"
                  onClick={closeModal}
                >
                  Close
                </button>
                <button
                  className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-full disabled:bg-gray-400"
                  type="button"
                  onClick={() => {
                    setIsLoading(true);
                    setTimeout(() => {
                      setIsLoading(false);
                      setOpenPayment(true);
                    }, 3000);
                  }}
                  disabled={isLoading}
                >
                  {isLoading ? 'Mohon menunggu' : 'Danai'}
                </button>
              </div>
            )}
          </div>
        </div>
      )}
    </ModalCustom>
  );
};

export default DashboardLenderModal;
