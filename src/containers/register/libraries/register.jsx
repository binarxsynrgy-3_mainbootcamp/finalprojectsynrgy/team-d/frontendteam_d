import { Box, Button, Link, Image } from '@chakra-ui/react';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { useRouter } from 'next/router';

import useSpinnerDispatch from '@/redux/spinner/dispatch';
import AuthRepository from '@/lib/repositories/auth';
import { alertSuccess } from '@/lib/hooks/toast';

const validationSchema = Yup.object({
  name: Yup.string().required('Nama harus diisi'),
  email: Yup.string()
    .email('format email tidak sesuai')
    .required('email harus diisi'),
  password: Yup.string()
    .required('password harus diisi')
    .min(8, 'password minimal 8 karakter'),
  passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Password tidak sesuai')
    .required('Password harus dikonfirmasi'),
  role: Yup.string().required('Harus pilih role'),
});

const initialValues = {
  email: '',
  name: '',
  password: '',
  passwordConfirmation: '',
  role: 'LENDER',
};

const RegisterContainer = () => {
  const router = useRouter();
  const { doHideSpinner, doShowSpinner, showSpinner } = useSpinnerDispatch();

  const handleOnSubmit = async (values) => {
    const { email, name, password, role } = values;
    doShowSpinner(true);
    const payload = {
      email,
      name,
      password,
      role,
    };
    const data = await AuthRepository().register(payload);
    doHideSpinner(false);
    if (data) {
      alertSuccess('Berhasil mendaftar, anda akan di arahkan ke halaman login');
      setTimeout(() => {
        router.push('/login');
      }, 3000);
    }
  };

  return (
    <div className="h-screen">
      <Box className="flex mx-auto justify-between my-24 px-16">
        <Box className="place-content-center w-96">
          <h1 className="pb-8 text-4xl text-center font-bold">Daftar</h1>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
              values,
            }) => (
              <form
                className="form__container space-y-2"
                onSubmit={handleSubmit}
              >
                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="name">
                    <input
                      name="name"
                      id="name"
                      placeholder="Nama Lengkap"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.name}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.name && (
                      <span
                        className={`text-red-500 form__control-message ${
                          touched.name && errors.name ? 'error' : ''
                        }`}
                      >
                        {touched.name && errors.name && errors.name}
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="email">
                    <input
                      name="email"
                      id="email"
                      placeholder="Email"
                      className="w-full outline-none pt-3 pb-1"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.email && (
                      <span
                        className={`text-red-500 form__control-message ${
                          touched.email && errors.email ? 'error' : ''
                        }`}
                      >
                        {touched.email && errors.email && errors.email}
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full text-sm">
                  <label className="block border-b w-full" htmlFor="password">
                    <input
                      type="password"
                      name="password"
                      id="password"
                      value={values.password}
                      placeholder="Password"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.password && (
                      <span
                        className={`form__control-message ${
                          touched.password && errors.password
                            ? 'text-red-500'
                            : ''
                        }`}
                      >
                        {touched.password && errors.password && errors.password}
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full text-sm">
                  <label
                    className="block border-b w-full"
                    htmlFor="passwordConfirmation"
                  >
                    <input
                      type="password"
                      name="passwordConfirmation"
                      id="passwordConfirmation"
                      value={values.passwordConfirmation}
                      placeholder="Konfirmasi Password"
                      className="w-full outline-none pt-3 pb-1"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </label>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.passwordConfirmation && (
                      <span
                        className={`form__control-message ${
                          touched.passwordConfirmation &&
                          errors.passwordConfirmation
                            ? 'text-red-500'
                            : ''
                        }`}
                      >
                        {touched.passwordConfirmation &&
                          errors.passwordConfirmation}
                      </span>
                    )}
                  </div>
                </div>

                <div className="w-full text-sm">
                  <div
                    role="group"
                    aria-labelledby="radio-group"
                    className="flex flex-row gap-8"
                  >
                    <label htmlFor="lender">
                      <Field
                        id="lender"
                        type="radio"
                        name="role"
                        value="LENDER"
                        className="mr-1"
                      />
                      Lender
                    </label>
                    <label htmlFor="borrower">
                      <Field
                        id="borrower"
                        type="radio"
                        name="role"
                        value="BORROWER"
                        className="mr-1"
                      />
                      Borrower
                    </label>
                  </div>
                  <div className="h-4 w-full text-xs">
                    {errors && errors.role && (
                      <span
                        className={`form__control-message ${
                          touched.role && errors.role ? 'text-red-500' : ''
                        }`}
                      >
                        {touched.role && errors.role}
                      </span>
                    )}
                  </div>
                </div>
                <div className="form__group form__actions">
                  <div>
                    <Button
                      w="100%"
                      colorScheme="cafund"
                      px={8}
                      type="submit"
                      mt={4}
                      disabled={showSpinner}
                    >
                      {showSpinner ? 'Mohon tunggu...' : 'Daftar'}
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
          <Box>
            <p className="mt-5 text-center">
              Sudah punya akun?
              <Link
                href="/login"
                color="blue.500"
                fontWeight="bold"
                marginLeft="1"
              >
                Masuk
              </Link>
            </p>
          </Box>
        </Box>

        <div className="flex flex-col items-center justify-center">
          <Box position="relative">
            <Image
              alt="Hero Image"
              fit="cover"
              align="center"
              w="100%"
              h="100%"
              src="/images/register.png"
            />
          </Box>
        </div>
      </Box>
    </div>
  );
};

export default RegisterContainer;
