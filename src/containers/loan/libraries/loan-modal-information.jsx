import { Dialog, Transition } from '@headlessui/react';
import { Fragment, useState } from 'react';

export function ModalInforomation() {
  const [isOpen, setIsOpen] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="bg-cafund-primary h-10 text-white font-bold py-2 px-16 rounded-md"
      >
        Detail
      </button>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-2/5 p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-cafund-base shadow-xl rounded-2xl">
                {/* Modal Detail Informasi Pembayaran */}
                <Dialog.Title className="text-lg font-bold text-cafund-primary">
                  Detail Informasi
                </Dialog.Title>
                <div className="mt-4 flex flex-col gap-5">
                  {/*Box  informasi tagihan */}
                  {/* {bank.map((bankItem) => (bankItem.checked? : ))}   */}
                  <div className="flex flex-col gap-2 p-3 w-full bg-white rounded-lg">
                    <div className="text-md text-gray-500">
                      Tagihan Bulan November
                    </div>
                    <div className=" text-4xl font-bold text-cafund-primary">
                      Rp. 1.688.300
                    </div>
                    <div className="text-md mt-3 text-cafund-primary">
                      Anda sudah membayar tagihan bulan ini
                    </div>
                  </div>

                  {/*Box Progress Pembayaran */}
                  <div className="p-3 w-full bg-white rounded-lg">
                    <div className="text-md text-gray-500">
                      Progress Pembayaran
                    </div>
                    <div className="mt-4 text-lg text-cafund-primary">
                      1 dari 5 bulan telah dibayar
                    </div>
                    <div className="bg-gray-300 w-full mt-4 h-2 rounded-lg">
                      <div className="bg-cafund-primary w-1/5 h-full rounded-lg" />
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}

export function ModalPaidOff() {
  const [isOpen, setIsOpen] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="bg-cafund-primary h-10 text-white font-bold py-2 px-16 rounded-md"
      >
        Detail
      </button>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-2/5 p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-cafund-base shadow-xl rounded-2xl">
                {/* Modal Detail Informasi Pembayaran */}
                <Dialog.Title className="text-lg font-bold text-cafund-primary">
                  Detail Informasi
                </Dialog.Title>
                <div className="mt-4 flex flex-col gap-5">
                  {/*Box Progress Pembayaran */}
                  <div className="p-3 w-full bg-white rounded-lg">
                    <div className="text-md text-gray-500">
                      Pembayaran Lunas
                    </div>
                    <div className="mt-4 text-lg text-cafund-primary">
                      5 dari 5 bulan telah dibayar
                    </div>
                    <div className="bg-gray-300 w-full mt-4 h-2 rounded-lg">
                      <div className="bg-cafund-primary w-full h-full rounded-lg" />
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
