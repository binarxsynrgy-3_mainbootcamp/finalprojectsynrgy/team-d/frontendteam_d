import { getLocalStorageValue } from '@/lib/hooks/useStorage';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import NumberFormat from 'react-number-format';
import { ContractModal } from './contract-modal';
import LoanModalDp from './loan-modal-dp';
import { ModalPayment } from './payment-modal';

const Loan = () => {
  const [myClasses, setMyClasses] = useState([]);
  const [openModalContract, setOpenModalContract] = useState(false);
  const [openModalPayment, setopenModalPayment] = useState(false);
  const [openModalDP, setopenModalDP] = useState(false);
  const [selectedClass, setSelectedClass] = useState(true);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const myClass = getLocalStorageValue('myClass');
      if (myClass) {
        setMyClasses(myClass);
      }
    }
  }, []);

  const loadClass = () => {
    const myClass = getLocalStorageValue('myClass');
    if (myClass) {
      setMyClasses(myClass);
    }
  };

  const loanStatus = (classItem) => {
    if (classItem.isDpPaid) {
      if (classItem.isAgreement) {
        if (classItem.isFunded) {
          return 'Didanai';
        } else {
          return 'Belum didanai';
        }
      } else {
        return '-';
      }
    }
    return 'Menunggu Pembayaran DP';
  };

  const btnLoan = (classItem) => {
    const className =
      'text-md bg-cafund-primary h-10 text-white font-semibold py-2 px-4 rounded-md disabled:bg-gray-500';
    const minWidth = { minWidth: '150px' };

    if (classItem.isDpPaid) {
      if (classItem.isAgreement) {
        if (classItem.isFunded) {
          return (
            <button
              type="button"
              className={className}
              style={minWidth}
              onClick={() => {
                setSelectedClass(classItem);
                setopenModalPayment(true);
              }}
            >
              Bayar
            </button>
          );
        } else {
          return (
            <button
              type="button"
              className={className}
              disabled
              style={minWidth}
            >
              Bayar
            </button>
          );
        }
      } else {
        return (
          <button
            type="button"
            className={className}
            style={minWidth}
            onClick={() => {
              setSelectedClass(classItem);
              setOpenModalContract(true);
            }}
          >
            <p className="text-md">Setujui Kontrak</p>
          </button>
        );
      }
    }
    return (
      <button
        type="button"
        className={className}
        style={minWidth}
        onClick={() => {
          setSelectedClass(classItem);
          setopenModalDP(true);
        }}
      >
        <p className="text-md">Bayar</p>
      </button>
    );
  };

  return (
    <div className="min-h-screen pb-8">
      {myClasses && myClasses.length > 0 ? (
        <div className="flex flex-col gap-4">
          {myClasses.map((classItem, index) => (
            <div
              key={index}
              className="flex gap-4 w-full items-center bg-white p-4"
            >
              <Image
                width={95}
                height={65}
                className="inline-block"
                src="/images/logo-binar.png"
                alt=""
              />
              <div className="flex-auto w-64">
                <div className="text-md font-bold mt-1">
                  {classItem.bootcampName}
                </div>
                <div className="text-md">{classItem.className}</div>
                <div className="text-md font-bold text-cafund-primary">
                  <NumberFormat
                    value={classItem.price}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp'}
                  />
                </div>
              </div>
              <div>
                <div className="text-md text-gray-500">Lama Cicilan</div>
                <div className="text-md font-bold text-cafund-primary">
                  {classItem.tenor} Bulan
                </div>
              </div>
              <div>
                <div className="text-md text-gray-500">Bunga</div>
                <div className="text-md font-bold text-cafund-primary">
                  {classItem.interestPercent * 100}%
                </div>
              </div>
              <div>
                <div className="text-md text-gray-500">Status Pinjaman</div>
                <div className="text-md font-bold text-cafund-primary">
                  {loanStatus(classItem)}
                </div>
              </div>
              <div className="flex items-center">{btnLoan(classItem)}</div>
            </div>
          ))}
        </div>
      ) : (
        <div className="text-cafund-primary bg-white h-40 flex items-center justify-center">
          Belum ada kelas yang di ambil :(
        </div>
      )}
      {openModalContract && (
        <ContractModal
          isOpen={openModalContract}
          closeModal={() => {
            loadClass();
            setOpenModalContract(false);
          }}
          classItem={selectedClass}
        />
      )}

      {openModalPayment && (
        <ModalPayment
          isOpen={openModalPayment}
          closeModal={() => {
            loadClass();
            setopenModalPayment(false);
          }}
          classItem={selectedClass}
        />
      )}

      {openModalDP && (
        <LoanModalDp
          classDetail={selectedClass}
          isOpen={openModalDP}
          closeModal={() => {
            loadClass();
            setopenModalDP(false);
          }}
        />
      )}
    </div>
  );
};

export default Loan;
