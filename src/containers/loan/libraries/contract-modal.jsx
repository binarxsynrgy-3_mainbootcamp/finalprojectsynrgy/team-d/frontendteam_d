import ModalCustom from '@/components/ModalCustom';
import { alertSuccess } from '@/lib/hooks/toast';
import {
  getLocalStorageValue,
  setLocalStorageValue,
} from '@/lib/hooks/useStorage';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { saveAs } from 'file-saver';
import Image from 'next/image';
import { useState } from 'react';

export function ContractModal({ isOpen, closeModal, classItem }) {
  const { doShowSpinner, doHideSpinner } = useSpinnerDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isChecked, setIsChecked] = useState(false);

  const downloadContract = () => {
    doShowSpinner();
    setTimeout(() => {
      doHideSpinner();
      saveAs(
        'https://careerfund.s3.ap-southeast-1.amazonaws.com/documents/bonds/bond.pdf',
        'Contract.pdf',
      );
    }, 3000);
  };

  const handleSubmit = () => {
    const myClass = getLocalStorageValue('myClass');
    myClass.forEach((item) => {
      if (
        item.bootcampName === classItem.bootcampName &&
        item.className === classItem.className
      ) {
        item.isAgreement = true;
      }
    });
    setLocalStorageValue('myClass', myClass);
    alertSuccess('Berhasil menyimpan data');
    closeModal();
  };

  return (
    <ModalCustom
      closeModal={closeModal}
      title="Detail Informasi"
      isOpen={isOpen}
    >
      <div className="p-3 w-full bg-white rounded-lg">
        <div className="text-lg text-gray-500">
          Surat Perjanjian Hutang Piutang
        </div>
        <div
          className="flex justify-between gap-6 p-3 my-9 mb-11 mx-auto w-80 h-20 border-2 border-cafund-primary bg-white rounded-lg cursor-pointer"
          onClick={downloadContract}
        >
          <div className="text-xl font-bold text-cafund-primary">
            Baca Surat Perjanjian Hutang Piutang
          </div>
          <Image
            width={95}
            height={65}
            className="inline-block"
            src="/images/file-text.png"
            alt=""
          />
        </div>
      </div>
      <label className="inline-flex items-center mt-2">
        <input
          type="checkbox"
          className="form-checkbox h-5 w-5 text-cafund-primary"
          value="agree"
          onChange={() => setIsChecked(!isChecked)}
        />
        <span className="ml-2 my-2 text-black">
          Saya telah membaca dan menyetujui surat perjanjian di atas
        </span>
      </label>
      <div className="flex items-center justify-center gap-2 p-4 border-blueGray-200 rounded-b">
        <button
          className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
          type="button"
          onClick={() => {
            setIsLoading(true);
            setTimeout(() => {
              setIsLoading(false);
              handleSubmit();
            }, 3000);
          }}
          disabled={isLoading || !isChecked}
        >
          {isLoading ? 'Submitting...' : 'Setujui'}
        </button>
      </div>
    </ModalCustom>
  );
}
