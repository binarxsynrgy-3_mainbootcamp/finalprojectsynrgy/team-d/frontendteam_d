import Header from '@/components/Header';
import PrivateLayout from '@/layouts/Private/libraries/PrivateLayout';
import PublicLayout from '@/layouts/Public/libraries/PublicLayout';
import { getLocalStorageValue } from '@/lib/hooks/useStorage';
import useAuthDispatch from '@/redux/auth/dispatch';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { useEffect } from 'react';
import HashLoader from 'react-spinners/HashLoader';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Layout = ({ children }) => {
  const { isLogged, doLogin, doLogout } = useAuthDispatch();
  const { showSpinner } = useSpinnerDispatch();

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const authentication = getLocalStorageValue('authentication');
      if (authentication) {
        doLogin(authentication);
      }
    }
  }, [isLogged]);

  return (
    <>
      <Header />
      {showSpinner && (
        <div>
          <div className="fixed inset-1/2" style={{ zIndex: 99 }}>
            <HashLoader color="#007AFF" loading={showSpinner} size={125} />
          </div>
          <div
            className="absolute bg-gray-800 opacity-25 w-full h-full"
            style={{ zIndex: 99 }}
          />
        </div>
      )}
      <ToastContainer />
      {isLogged ? (
        <PrivateLayout>{children}</PrivateLayout>
      ) : (
        <PublicLayout>{children}</PublicLayout>
      )}
    </>
  );
};

export default Layout;
