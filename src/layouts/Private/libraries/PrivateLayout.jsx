import Navbar from '@/components/Navbar';
import Sidebar from '@/components/Sidebar';
import useAuthDispatch from '@/redux/auth/dispatch';

const PrivateLayout = ({ children }) => {
  const { user } = useAuthDispatch();
  const { roles } = user;

  return (
    <div className="h-full bg-cafund-base">
      <Navbar />
      <div className="max-w-7xl m-auto mt-8">
        <div className="flex fle-row gap-6">
          {roles.includes('ROLE_BORROWER') && (
            <div className="flex-none w-60">
              <Sidebar />
            </div>
          )}
          <div className="h-full w-full">
            <div className="w-full text-grey-darkest h-full">{children}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrivateLayout;
