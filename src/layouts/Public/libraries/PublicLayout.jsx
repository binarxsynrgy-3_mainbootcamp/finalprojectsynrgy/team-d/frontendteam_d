import Navbar from '@/components/Navbar';

const PublicLayout = ({ children }) => {

  return (
    <div>
      <Navbar />
      <div className="max-w-7xl m-auto">
        {children}
      </div>
    </div>
  );
};

export default PublicLayout;
