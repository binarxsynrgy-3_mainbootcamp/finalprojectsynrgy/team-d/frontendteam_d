import RegisterContainer from '@/containers/register/libraries/register';
import Layout from '@/layouts/Layout';

const Register = () => (
  <Layout>
    <RegisterContainer />
  </Layout>
);

export default Register;
