import BorrowerClass from '@/containers/borrower-class';
import Layout from '@/layouts/Layout/libraries/Layout';

const ClassPage = () => (
  <Layout>
    <BorrowerClass />
  </Layout>
);

export default ClassPage;
