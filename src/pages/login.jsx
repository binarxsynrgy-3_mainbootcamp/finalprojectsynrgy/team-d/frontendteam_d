import LoginContainer from '@/containers/login/libraries/login';
import Layout from '@/layouts/Layout';

const Login = () => (
  <Layout>
    <LoginContainer />
  </Layout>
);

export default Login;
