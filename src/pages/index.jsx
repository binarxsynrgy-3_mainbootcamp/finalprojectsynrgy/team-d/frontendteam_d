import DashboardBorrower from '@/containers/dashboard-borrower';
import DashboardLender from '@/containers/dashboard-lender';
import LandingContainer from '@/containers/landing';
import Layout from '@/layouts/Layout';
import useAuthDispatch from '@/redux/auth/dispatch';

function Home() {
  const { isLogged, user } = useAuthDispatch();
  const { roles } = user;

  const dashboard = roles.includes('ROLE_LENDER') ? (
    <DashboardLender />
  ) : (
    <DashboardBorrower />
  );

  return <Layout>{!isLogged ? <LandingContainer /> : dashboard}</Layout>;
}

export default Home;
