import ModalCustom from '@/components/ModalCustom';
import Navbar from '@/components/Navbar';
import { ASSESMENT_QUESTIONS } from '@/lib/constant/assesment';
import { alertSuccess } from '@/lib/hooks/toast';
import {
  getLocalStorageValue,
  setLocalStorageValue,
} from '@/lib/hooks/useStorage';
import useAssementDispatch from '@/redux/assesment/dispatch';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Countdown from 'react-countdown';

const ModalAssesmentStart = ({ isOpen, closeModal }) => {
  return (
    <ModalCustom isOpen={isOpen} title="Petunjuk" closeModal={closeModal}>
      <div className="flex justify-center  items-center antialiased">
        <div className="flex flex-col  gap-4">
          <div className="flex flex-col px-6 py-5 bg-white">
            <div className="mb-2 text-sm text-gray-400">Informasi Asesmen</div>
            <div>
              <table className="table-auto gap-4">
                <tbody className="bg-white divide-y divide-gray-200">
                  <tr>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-center">
                        <div className="ml-4">
                          <div
                            className="text-left"
                            style={{ color: '#3163AF' }}
                          >
                            jumlah Soal
                          </div>
                          <div
                            className="text-left font-bold"
                            style={{ color: '#3163AF' }}
                          >
                            25 soal
                          </div>
                        </div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm" style={{ color: '#3163AF' }}>
                        Durasi Asesmen
                      </div>
                      <div
                        className="text-left font-bold"
                        style={{ color: '#3163AF' }}
                      >
                        40 menit
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm" style={{ color: '#3163AF' }}>
                        Jenis Soal
                      </div>
                      <div
                        className="text-left font-bold"
                        style={{ color: '#3163AF' }}
                      >
                        Pilihan Ganda
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm" style={{ color: '#3163AF' }}>
                        Passing Grade
                      </div>
                      <div
                        className="text-left font-bold"
                        style={{ color: '#3163AF' }}
                      >
                        8,5
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div className="flex flex-col px-6 py-5 bg-white">
            <div className="mb-4 text-sm text-gray-400">Catatan</div>
            <div className="text-center" style={{ color: '#3163AF' }}>
              Asesmen dapat diselesaikan sebelum durasi berakhir melalui tombol
              "Selesai"
            </div>
            <div className="px-44 flex flex-row items-center justify-between p-5 rounded-bl-lg rounded-br-lg">
              <button
                className="text-white bg-cafund-primary rounded"
                style={{
                  minWidth: 200,
                  minHeight: 40,
                  borderRadius: 7,
                }}
                onClick={closeModal}
              >
                Mulai Asesmen
              </button>
            </div>
          </div>
        </div>
      </div>
    </ModalCustom>
  );
};

const initialAnswer = [null, null, null, null, null];

const AssesmentPage = () => {
  const router = useRouter();
  const { assesment, setStartTime, setAssement } = useAssementDispatch();
  const [assesmentAsnwer, setAssementAnswer] = useState(initialAnswer);
  const [openDirectionModal, setOpenDirectionModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [openModalFinish, setOpenModalFinish] = useState(false);

  const startAsessment = () => {
    if (assesment.startTime === null) {
      const now = Date.now();
      setStartTime(now);
      setLocalStorageValue('assesment', {
        startTime: now,
        directionOpened: false,
      });
    }
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const value = getLocalStorageValue('assesment');
      if (value) {
        setStartTime(value.startTime);
      } else {
        setOpenDirectionModal(true);
      }
    }
  }, []);

  useEffect(() => {
    window.addEventListener('beforeunload', alertUser);
    return () => {
      window.removeEventListener('beforeunload', alertUser);
    };
  }, []);

  const alertUser = (e) => {
    e.preventDefault();
    e.returnValue = '';
  };

  const calculateResult = () => {
    const result = assesmentAsnwer.reduce((acc, curr, index) => {
      if (curr === ASSESMENT_QUESTIONS[index].correctAnswer) {
        return acc + 1;
      }
      return acc;
    }, 0);
    return result;
  };

  const submitAssesment = () => {
    setIsLoading(true);
    const testScores = (calculateResult() / ASSESMENT_QUESTIONS.length) * 100;
    const result = {
      testScores,
      isDone: true,
      workDate: assesment.startTime,
      startTime: null,
    };
    setTimeout(() => {
      alertSuccess('Berhasil mengirimkan asesmen');
      setLocalStorageValue('assesment', result);
      setAssement(result);
      setIsLoading(false);
      router.push('/');
    }, 3000);
  };

  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return (
        <p className="p-2 text-sm text-grey text-center font-bold">00:00</p>
      );
    } else {
      return (
        <p className="p-2 text-sm text-grey text-center font-bold">
          {minutes < 10 ? `0${minutes}` : minutes}:
          {seconds < 10 ? `0${seconds}` : seconds}
        </p>
      );
    }
  };

  const onchangeOptions = (questionIndex, key) => {
    assesmentAsnwer[questionIndex] = key;
    setAssementAnswer([...assesmentAsnwer]);
  };

  return (
    <div className="h-full pb-8">
      <Navbar />
      <div
        className="w-full h-full px-8 "
        style={{ backgroundColor: '#F2F2F7' }}
      >
        <div
          className="flex pt-5 mx-auto justify-center container"
          style={{ backgroundColor: '#F2F2F7' }}
        >
          <div className="flex-none" />
          <p className=" text-2xl text-left text-cafund-primary md:text-cafund-primary font-bold">
            Assessment
          </p>
          <div className="flex-grow h-16 ..." />
          <div className="grid grid-cols-2">
            {assesment && assesment.startTime !== null && (
              <Countdown
                date={assesment.startTime + 60000 * 40}
                renderer={renderer}
              />
            )}
            <div>
              <button
                href="#"
                className="bg-cafund-primary hover:bg-cafund-primary text-white text-center rounded focus:outline-none focus:shadow-outline"
                style={{ minWidth: 150, minHeight: 30, borderRadius: 7 }}
                type="submit"
                onClick={() => setOpenModalFinish(true)}
              >
                Selesai
              </button>
            </div>
          </div>
        </div>
        {ASSESMENT_QUESTIONS &&
          ASSESMENT_QUESTIONS.map((question, questionIndex) => (
            <div
              key={questionIndex}
              className="p-4 bg-white container sm:rounded-lg items-center mx-auto justify-center"
            >
              <div>
                <p className="mb-4 font-semibold text-xl text-left  ">
                  <b>{questionIndex + 1}.</b> &ensp; {question.question}
                </p>
              </div>
              <div className="ml-12 static grid md:grid-cols-1 gap-4">
                {Object.keys(question.options).map((key, optionIndex) => (
                  <div
                    key={optionIndex}
                    className="flex flex-row justify-start items-center gap-4"
                  >
                    <input
                      type="radio"
                      name={'option-' + questionIndex}
                      value={key}
                      onChange={() => onchangeOptions(questionIndex, key)}
                    />
                    <div className="">
                      {key.toUpperCase()}. {question.options[key]}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          ))}
      </div>
      <>
        {openModalFinish ? (
          <ModalCustom
            isOpen={openModalFinish}
            title="Konfirmasi"
            closeModal={() => setOpenModalFinish(false)}
          >
            <div className="w-full">
              <p className="my-4 text-blueGray-500 text-lg leading-relaxed">
                Apakah kamu yakin ingin menyelesaikan assessment ini?
              </p>
            </div>
            <div className="flex items-center justify-center gap-2 p-4 border-blueGray-200 rounded-b">
              <button
                className="bg-white font-bold py-2 px-4 rounded-md w-40"
                type="button"
                onClick={() => setOpenModalFinish(false)}
              >
                Close
              </button>
              <button
                className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
                type="button"
                onClick={() => submitAssesment()}
                disabled={isLoading}
              >
                {isLoading ? 'Mengirimkan...' : 'Kirim'}
              </button>
            </div>
          </ModalCustom>
        ) : null}
      </>

      {openDirectionModal && (
        <ModalAssesmentStart
          isOpen={openDirectionModal}
          closeModal={() => {
            setOpenDirectionModal(false);
            startAsessment();
          }}
        />
      )}
    </div>
  );
};

export default AssesmentPage;
