import DashboardLender from '@/containers/dashboard-lender';
import Layout from '@/layouts/Layout/libraries/Layout';

function DashboardLenderPage() {
  return (
    <Layout>
      <DashboardLender />
    </Layout>
  );
}

export default DashboardLenderPage;
