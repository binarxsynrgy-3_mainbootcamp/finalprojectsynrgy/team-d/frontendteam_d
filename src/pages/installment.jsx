import InstallmentContainer from '@/containers/installment';
import Layout from '@/layouts/Layout';

const InstallmentPage = () => {
  return (
    <Layout>
      <InstallmentContainer />
    </Layout>
  );
};

export default InstallmentPage;
