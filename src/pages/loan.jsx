import Loan from '@/containers/loan/libraries/loan';
import Layout from '@/layouts/Layout/libraries/Layout';

function LoanPage() {
  return (
    <Layout>
      <Loan />
    </Layout>
  );
}

export default LoanPage;
