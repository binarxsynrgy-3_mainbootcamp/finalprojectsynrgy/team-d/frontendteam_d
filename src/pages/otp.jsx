import OtpContainer from '@/containers/otp/libraries/otp';
import Layout from '@/layouts/Layout';

const Otp = () => (
  <Layout>
    <OtpContainer />
  </Layout>
);

export default Otp;
