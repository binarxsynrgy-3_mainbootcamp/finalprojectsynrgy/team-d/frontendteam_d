import BorrowerInformation from '@/containers/borrower-information';
import Layout from '@/layouts/Layout';

const BorrowerInformationPage = () => (
  <Layout>
    <BorrowerInformation />
  </Layout>
);

export default BorrowerInformationPage;
