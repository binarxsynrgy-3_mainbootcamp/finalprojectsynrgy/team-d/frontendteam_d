import Layout from '@/layouts/Layout';

const DashboardAssesmentPage = () => (
  <Layout>
    <h1 id="Assessment" />
    <div
      className="w-3/5 md:w-3/5 lg:w-full"
      style={{ backgroundColor: '#F2F2F7' }}
    >
      <div
        className="flex pt-5  border mx-auto justify-right"
        style={{ backgroundColor: '#F2F2F7' }}
      >
        <div className="flex-none" />
        <p className=" text-2xl text-left text-cafund-primary md:text-cafund-primary font-bold">
          Dashboard
        </p>
        <div className="flex-grow h-16 ..." />
        <div className="grid grid-cols-1 md:grid-cols-1">
          <div>
            <p className="text-xs text-grey text-left">Total Aset</p>
          </div>
          <div>
            <p
              className="mb-6 text-1xl text-right text-cafund-primary md:text-cafund-primary font-bold"
              style={{ textAlign: 'right' }}
            >
              Rp 4.320.000,-
            </p>
          </div>
        </div>
      </div>

      {/* Nilai Assessment */}
      <div className="border  p-8 bg-white container sm:rounded-lg items-center mx-auto justify-center">
        <div>
          <div className="flex flex-col bg-white">
            <div className="mb-2 text-xl text-gray-500">Nilai Asesmen</div>
            <div
              className="mb-4 text-5xl font-bold"
              style={{ color: '#3163AF' }}
            >
              8,5
            </div>
            <div>
              <button
                className="bg-cafund-primary hover:bg-gray-600 mt-6 text-white text-sm text-center rounded focus:outline-none focus:shadow-outline mx-auto"
                style={{ minWidth: 160, minHeight: 30 }}
                type="submit"
              >
                Ulangi Asesmen
              </button>
            </div>
            {/* Buttun di bawah akan muncul ketika udah diklik tombol Assessment di atas */}
            {/* <div>
              <button
                className="bg-gray-600 text-white text-sm text-center rounded focus:outline-none focus:shadow-outline mx-auto"
                style={{ minWidth: 160, minHeight: 30 }}
                type="submit"
              >
                Ulangi Asesmen
              </button>
            </div>
            <div className="pt-2">
              <p className="text-xs text-gray-500">
                Bisa dilakukann mulai 10 September 2021
              </p>
            </div> */}
          </div>
        </div>
      </div>

      {/* Kelas Yang Sudah Diambil */}
      <div
        className="border  pt-8 pb-3 container sm:rounded-lg items-center mx-auto justify-center"
        style={{ backgroundColor: '#F2F2F7' }}
      >
        <div
          className=" text-base text-left font-bold"
          style={{ color: '#3163AF' }}
        >
          Kelas Yang Sudah Diambil
        </div>
      </div>
      <div className="flex flex-col  pb-8 mx-auto justify-center">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full md:w-full lg:w-full divide-y divide-gray-200">
                <tbody className="bg-white divide-y divide-gray-200">
                  <tr>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <img
                        width={50}
                        height={50}
                        src="/img/Binar.png"
                        style={{
                          alignSelf: 'flex-center',
                        }}
                      />
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-left">
                        <div className="ml-4">
                          <div className="text-left font-bold">
                            Binar Academy
                          </div>
                          <div className="text-left">Back End Developer</div>
                        </div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm">Tanggal Bootcamp</div>
                      <div
                        className="text-left font-bold"
                        style={{ color: '#3163AF' }}
                      >
                        April-Juni 2021
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm">Nilai</div>
                      <div
                        className="text-left font-bold"
                        style={{ color: '#3163AF' }}
                      >
                        8,7
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Layout>
);

export default DashboardAssesmentPage;
