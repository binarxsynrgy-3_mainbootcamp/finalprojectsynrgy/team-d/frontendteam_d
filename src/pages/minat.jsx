import MinatContainer from '@/containers/minat';
import Navbar from '@/components/Navbar';

export default function Minat() {
  return (
    <>
      <Navbar />
      <MinatContainer />
    </>
  );
}
