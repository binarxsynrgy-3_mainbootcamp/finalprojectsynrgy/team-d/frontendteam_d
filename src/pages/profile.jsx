import ModalCostumProfile from '@/components/ModalCostumProfile';
import Layout from '@/layouts/Layout';
import { alertSuccess } from '@/lib/hooks/toast';
import AuthRepository from '@/lib/repositories/auth';
import useModalDispatch from '@/redux/modal/dispatch';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { Box, Image } from '@chakra-ui/react';
import { Formik } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import { useRef, useState } from 'react';
import { setLocalStorageValue } from '@/lib/hooks/useStorage';

const validationSchema = Yup.object({
  name: Yup.string().required('Nama harus diisi'),
  email: Yup.string()
    .email('format email tidak sesuai')
    .required('Email Harus Diisi'),
  noHp: Yup.string().required('No HP Harus Diisi'),
  alamat: Yup.string().required('Alamat Harus Diisi'),
});

const initialValues = {
  email: '',
  name: '',
  noHp: '',
  alamat: '',
};

const ProfileContainer = () => {
  const router = useRouter();
  const { showModal, doShowModal } = useModalDispatch();
  const { doHideSpinner, doShowSpinner, showSpinner } = useSpinnerDispatch();

  const [KTPImageUrl, setKTPImageUrl] = useState(null);
  const [photoWithKTPImageUrl, setPhotoWithKTPImageUrl] = useState(null);
  const inputKTPImage = useRef(null);
  const inputPhotoWithKTPImage = useRef(null);

  const submitProfile = () => {
    console.log('tes');
    setLocalStorageValue('profile', { profile: 'profile' });
    router.push('/');
  };

  const handleOnSubmit = async (values) => {
    const { email, name, noHp, alamat } = values;
    doShowSpinner(true);
    const payload = {
      email,
      name,
      noHp,
      alamat,
    };
    const data = await AuthRepository().register(payload);
    doHideSpinner(false);
    if (data) {
      alertSuccess('Berhasil mendaftar, anda akan di arahkan ke halaman login');
      setTimeout(() => {
        router.push('/login');
      }, 3000);
    }
  };

  const onUploadButtonClick = (e, type) => {
    if (type == 'KTP') {
      inputKTPImage.current.click();
    } else {
      inputPhotoWithKTPImage.current.click();
    }
    console.log(inputKTPImage);
    console.log(inputPhotoWithKTPImage);
    console.log(type);
  };

  const inputImage = (e, type) => {
    const files =
      type == 'KTP'
        ? inputKTPImage.current?.files
        : inputPhotoWithKTPImage.current?.files;
    var reader = new FileReader();
    if (type == 'KTP') {
      reader.onload = function (e) {
        setKTPImageUrl(e.target.result);
      };
    } else {
      reader.onload = function (e) {
        setPhotoWithKTPImageUrl(e.target.result);
      };
    }
    reader.readAsDataURL(files[0]);
  };

  return (
    <Layout>
      <div style={{ backgroundColor: '#F2F2F7' }}>
        {/* Bagian Judul */}
        <div
          className="w-3/5 md:w-3/5 lg:w-full"
          style={{ backgroundColor: '#F2F2F7' }}
        >
          <div
            className="flex pt-5  mx-auto justify-right"
            style={{ backgroundColor: '#F2F2F7' }}
          >
            <div className="flex-none" />
            <p className=" text-2xl text-left text-cafund-primary md:text-cafund-primary font-bold">
              Data Diri
            </p>
            <div className="flex-grow h-16 ..." />
          </div>
        </div>

        <div className="p-8 w-full h-screen bg-white content-around rounded-xl">
          <Box className="flex mx-auto">
            <Box className="place-content-center w-full grid-cols-9">
              <Formik
                initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={validationSchema}
              >
                {({
                  handleSubmit,
                  handleChange,
                  handleBlur,
                  touched,
                  errors,
                  values,
                }) => (
                  <form
                    className="form__container space-y-2 grid-cols-9"
                    onSubmit={handleSubmit}
                  >
                    <div className="w-full text-sm">
                      <label
                        className="hud-prefix text-gray-500 block border-b w-full"
                        htmlFor="name"
                      >
                        {' '}
                        Nama Lengkap
                        <input
                          name="name"
                          id="name"
                          placeholder=""
                          className="w-full outline-none pt-3 pb-1"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.name}
                        />
                      </label>
                      <div className="h-4 w-full text-xs">
                        {errors && errors.name && (
                          <span
                            className={`text-red-500 form__control-message ${
                              touched.name && errors.name ? 'error' : ''
                            }`}
                          >
                            {touched.name && errors.name && errors.name}
                          </span>
                        )}
                      </div>
                    </div>

                    <div className="w-full text-sm">
                      <label
                        className="hud-prefix text-gray-500 block border-b w-full"
                        htmlFor="email"
                      >
                        {' '}
                        Email
                        <input
                          name="email"
                          id="email"
                          placeholder=""
                          className="w-full outline-none pt-3 pb-1"
                          value={values.email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </label>
                      <div className="h-4 w-full text-xs">
                        {errors && errors.email && (
                          <span
                            className={`text-red-500 form__control-message ${
                              touched.email && errors.email ? 'error' : ''
                            }`}
                          >
                            {touched.email && errors.email && errors.email}
                          </span>
                        )}
                      </div>
                    </div>

                    <div className="w-full text-sm">
                      <label
                        className="hud-prefix text-gray-500 block border-b w-full"
                        htmlFor="noHp"
                      >
                        No HP
                        <input
                          type="noHp"
                          name="noHp"
                          id="noHp"
                          value={values.noHp}
                          placeholder=""
                          className="w-full outline-none pt-3 pb-1"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </label>
                      <div className="h-4 w-full text-xs">
                        {errors && errors.noHp && (
                          <span
                            className={`form__control-message ${
                              touched.noHp && errors.noHp ? 'text-red-500' : ''
                            }`}
                          >
                            {touched.noHp && errors.noHp && errors.noHp}
                          </span>
                        )}
                      </div>
                    </div>

                    <div className="w-full text-sm">
                      <label
                        className="hud-prefix text-gray-500 block border-b w-full"
                        htmlFor="alamat"
                      >
                        Alamat
                        <input
                          type="alamat"
                          name="alamat"
                          id="alamat"
                          value={values.alamat}
                          placeholder=""
                          className="w-full outline-none pt-3 pb-1"
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </label>
                      <div className="h-4 w-full text-xs">
                        {errors && errors.alamat && (
                          <span
                            className={`form__control-message ${
                              touched.alamat && errors.alamat
                                ? 'text-red-500'
                                : ''
                            }`}
                          >
                            {touched.alamat && errors.alamat}
                          </span>
                        )}
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            </Box>
            <div className="grid-cols-3 gap-4">
              <div className="pb-2 text-base font-bold">Foto KTP</div>

              <Image
                className="bg-gray-300"
                width="177px"
                height="140px"
                src={KTPImageUrl}
                alt=""
                style={{ borderRadius: 15 }}
              ></Image>

              <div
                className="bg-gray-400 text-gray-100 text-center item-center"
                style={{
                  marginTop: -50,
                  height: 50,
                  borderRadius: 7,
                  borderBottomLeftRadius: 15,
                  borderBottomRightRadius: 15,
                }}
                type="button"
              >
                <input
                  type="file"
                  id="file"
                  ref={inputKTPImage}
                  onInput={(e) => inputImage(e, 'KTP')}
                  style={{ display: 'none' }}
                />

                <button
                  className="pt-3 bg-gray-400 text-gray-100"
                  onClick={(e) => onUploadButtonClick(e, 'KTP')}
                >
                  Unggah Foto
                </button>
              </div>
              <div className="pt-5 pb-2 font-bold">Foto Bersama KTP</div>

              <Image
                className="bg-gray-300 rounded"
                width="177px"
                height="140px"
                src={photoWithKTPImageUrl}
                alt=""
                style={{ borderRadius: 15 }}
              ></Image>
              <div
                className="bg-gray-400 text-gray-100 text-center item-center"
                style={{
                  marginTop: -50,
                  height: 50,
                  borderRadius: 7,
                  borderBottomLeftRadius: 15,
                  borderBottomRightRadius: 15,
                }}
                type="submit"
              >
                <input
                  type="file"
                  id="photoWithKTP"
                  ref={inputPhotoWithKTPImage}
                  onInput={(e) => inputImage(e, 'PhotoWithKTP')}
                  style={{ display: 'none' }}
                />
                <button
                  className="pt-3 bg-gray-400 text-gray-100"
                  onClick={(e) => onUploadButtonClick(e, 'PhotoWithKTP')}
                >
                  Unggah Foto
                </button>
              </div>
            </div>
          </Box>
          <div className="pt-16 px-96 w-4/5 h-screen bg-white justify-center">
            <button
              href="#"
              className="bg-cafund-primary hover:bg-gray-700 text-white font-semibold pd-3 py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline items-center mx-auto justify-center"
              w="20%"
              colorScheme="cafund"
              px={8}
              type="submit"
              mt={4}
              disabled={showSpinner}
              alignItems="center"
              style={{ minWidth: 200 }}
              onClick={doShowModal}
            >
              {showSpinner ? 'Please wait...' : 'Verifikasi'}
            </button>
          </div>
        </div>
      </div>
      <>
        {showModal ? (
          <ModalCostumProfile
            className="text-center rounded-lg"
            title="Konfirmasi Data Diri"
            handleSubmit={submitProfile}
            style={{ borderRadius: 13 }}
          >
            <p className="pb-5 font-bold text-center">
              Apakah data yang Anda masukkan sudah benar?
            </p>
            <div
              className="px-28 items-center justify-center"
              style={{ width: '50%', height: '50%' }}
            >
              <div>
                <table className="bg-white table-auto gap-4 rounded-lg">
                  <tbody className="divide-y divide-gray-200">
                    <tr>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="ml-4">
                            <div className="text-left text-gray-400">Nama</div>
                            <div className="text-left text-gray-400">Email</div>
                            <div className="text-left text-gray-400">No HP</div>
                            <div className="text-left text-gray-400">
                              Alamat
                            </div>
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-right font-bold">
                          Mira Setiawan
                        </div>
                        <div className="text-right font-bold">
                          mirasetiawan12@gmail.com
                        </div>
                        <div className="text-right font-bold">0829898989</div>
                        <div className="text-right font-bold">
                          Jl. Jendral Soediman No 12, Semarang, 2313
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </ModalCostumProfile>
        ) : null}
      </>
    </Layout>
  );
};

export default ProfileContainer;
