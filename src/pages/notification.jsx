import Layout from '@/layouts/Layout';
import Notification from '@/containers/notif/libraries/notification';

const notificationPage = () => (
  <Layout>
    <Notification />
  </Layout>
);

export default notificationPage;
