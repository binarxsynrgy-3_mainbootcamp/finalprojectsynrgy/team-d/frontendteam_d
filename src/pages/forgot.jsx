import ForgotContainer from '@/containers/forgot';
import Navbar from '@/components/Navbar';

export default function Forgot() {
  return (
    <>
      <Navbar />
      <ForgotContainer />
    </>
  );
}
