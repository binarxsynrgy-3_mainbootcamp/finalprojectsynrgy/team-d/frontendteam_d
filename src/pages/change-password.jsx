/* eslint-disable @next/next/link-passhref */
import { Formik } from 'formik';
import * as Yup from 'yup';

const aboutPage = () => {
  const ChangePasswordSchema = Yup.object().shape({
    password: Yup.string()
      .required('password harus diisi')
      .min(8, 'password minimal 8 karakter'),
    passwordConfirmation: Yup.string()
      .oneOf([Yup.ref('password'), null], 'Password tidak sesuai')
      .required('Password harus dikonfirmasi'),
  });

  return (
    <div
      className="container border flex items-center mx-auto min-h-screen justify-center px-4"
      style={{ alignContent: 'center' }}
    >
      <main className="w-full">
        <div className="flex items-center  w-full md:w-3/5 justify-center mx-auto">
          <div className="w-5/6  md:w-full">
            <div className="w-full bg-white">
              <div
                className="border m-8 w-full md:w-1/2 pt-6 pb-8"
                style={{ alignSelf: 'center' }}
              >
                <p className="pb-8 text-5xl text-center font-bold">
                  Ganti Password
                </p>
                <Formik
                  initialValues={{
                    password: '',
                    passwordConfirmation: '',
                  }}
                  validationSchema={ChangePasswordSchema}
                  onSubmit={(values) => {
                    // eslint-disable-next-line no-console
                  }}
                >
                  {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    touched,
                    errors,
                  }) => (
                    <form
                      className="form__container space-y-2"
                      onSubmit={handleSubmit}
                    >
                      <div className="w-full text-sm">
                        <label
                          className="block border-b w-full"
                          htmlFor="password"
                        >
                          <input
                            type="password"
                            name="password"
                            id="password"
                            placeholder="Password Baru"
                            className="w-full outline-none pt-3 pb-1"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </label>
                        <div className="h-4 w-full text-xs">
                          {errors && errors.password && (
                            <span
                              className={`form__control-message ${
                                touched.password && errors.password
                                  ? 'text-red-500'
                                  : ''
                              }`}
                            >
                              {touched.password &&
                                errors.password &&
                                errors.password}
                            </span>
                          )}
                        </div>
                      </div>

                      <div className="w-full text-sm">
                        <label
                          className="block border-b w-full"
                          htmlFor="passwordConfirmation"
                        >
                          <input
                            type="password"
                            name="passwordConfirmation"
                            id="passwordConfirmation"
                            placeholder="Konfirmasi Password Baru"
                            className="w-full outline-none pt-3 pb-1"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </label>
                        <div className="h-4 w-full text-xs">
                          {errors && errors.passwordConfirmation && (
                            <span
                              className={`form__control-message ${
                                touched.passwordConfirmation &&
                                errors.passwordConfirmation
                                  ? 'text-red-500'
                                  : ''
                              }`}
                            >
                              {touched.passwordConfirmation &&
                                errors.passwordConfirmation}
                            </span>
                          )}
                        </div>
                      </div>
                      <button
                        className=" mt-6 mb-3 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full"
                        style={{ backgroundColor: '#3163AF' }}
                        type="submit"
                      >
                        Change Password
                      </button>
                    </form>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default aboutPage;
