import ModalCustom from '@/components/ModalCustom';
import Image from 'next/image';
import { useState } from 'react';

const ModalLogout = ({ isOpen, closeModal, handleSubmit }) => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <>
      <ModalCustom isOpen={isOpen} closeModal={closeModal}>
        <div className="py-8 w-full flex items-center justify-center">
          <Image width="100%" height="100%" src="/img/Out.png" alt="" />
        </div>
        <p className="pl-20 pr-20 pt-8 pb-5 font-bold text-center">
          Apakah Anda yakin akan keluar?
        </p>
        <div className="flex items-center justify-center gap-2 p-4 border-blueGray-200 rounded-b">
          <button
            className="bg-white font-bold py-2 px-4 rounded-md w-40"
            type="button"
            onClick={closeModal}
          >
            Close
          </button>
          <button
            className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
            type="button"
            onClick={() => {
              setIsLoading(true);
              handleSubmit();
            }}
            disabled={isLoading}
          >
            Keluar
          </button>
        </div>
      </ModalCustom>
    </>
  );
};

export default ModalLogout;
