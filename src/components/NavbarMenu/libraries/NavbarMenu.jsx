import classNames from 'classnames';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { IoCloseOutline, IoMenu } from 'react-icons/io5';
import { Menu, Transition } from '@headlessui/react';
import { Fragment } from 'react';

import { NAVBAR_PUBLIC, NAVBAR_LENDER } from '@/lib/constant/menu';
import useAuthDispatch from '@/redux/auth/dispatch';
import useSpinnerDispatch from '@/redux/spinner/dispatch';
import { useRouter } from 'next/router';
import { removeLocalStorageValue } from '@/lib/hooks/useStorage';
import ModalLogout from './modal-logout';
import { alertSuccess } from '@/lib/hooks/toast';

const dummyProfile =
  'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80';

const MenuToggle = ({ toggle, isOpen }) => (
  <div
    className="block md:hidden"
    role="button"
    tabIndex={0}
    onClick={toggle}
    onKeyDown={toggle}
  >
    {isOpen ? (
      <IoCloseOutline className="w-6 h-6" />
    ) : (
      <IoMenu className="w-6 h-6" />
    )}
  </div>
);

const MenuItem = ({ children, to = '/', currentPath = '/' }) => (
  <Link href={to} className="w-full" passHref>
    <div
      className={classNames(
        'text-gray-500 hover:text-cafund-primary text-left block font-bold text-md cursor-pointer w-full whitespace-nowrap',
        {
          'text-cafund-primary': currentPath === to,
        },
      )}
    >
      {children}
    </div>
  </Link>
);

const MenuLinks = ({ isOpen }) => {
  const router = useRouter();
  const { isLogged, user, doLogout } = useAuthDispatch();
  const { roles } = user;
  const [displayLenderMenu, setDisplayLenderMenu] = useState(false);
  const [displayPublicMenu, setDisplayPublicMenu] = useState(false);
  const [openModalLogout, setOpenModalLogout] = useState(false);
  const { doHideSpinner, doShowSpinner } = useSpinnerDispatch();

  useEffect(() => {
    if (roles.includes('ROLE_LENDER')) {
      setDisplayLenderMenu(true);
    } else {
      setDisplayLenderMenu(false);
    }

    if (!isLogged) {
      setDisplayPublicMenu(true);
    } else {
      setDisplayPublicMenu(false);
    }
  }, [roles, isLogged]);

  const logout = () => {
    doShowSpinner();
    alertSuccess('Berhasil Keluar');
    setTimeout(() => {
      removeLocalStorageValue('authentication');
      doLogout();
      router.push('/login');
      doHideSpinner();
    }, 3000);
  };

  return (
    <>
      <div
        className={classNames('md:block', {
          block: isOpen,
          hidden: !isOpen,
        })}
      >
        <div className="shadow-md md:shadow-none space-x-8 p-4 sm:p-4 md:p-0 flex flex-col sm:flex-row items-center justify-center sm:justify-between md:justify-end">
          {isLogged && (
            <div className="flex flex-row gap-8">
              {displayLenderMenu &&
                NAVBAR_LENDER.map((item) => (
                  <MenuItem
                    key={item.route}
                    to={item.route}
                    currentPath={router.pathname}
                  >
                    {item.label}
                  </MenuItem>
                ))}
              <Menu as="div" className="ml-3 relative w-full">
                <div>
                  <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    <span className="sr-only">Open user menu</span>
                    <img
                      className="h-8 w-8 rounded-full"
                      src={dummyProfile}
                      alt=""
                    />
                  </Menu.Button>
                </div>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <Menu.Item>
                      {({ active }) => (
                        <a
                          href="#"
                          className={classNames(
                            active ? 'bg-gray-100' : '',
                            'block px-4 py-2 text-sm text-gray-700',
                          )}
                          onClick={(e) => {
                            e.preventDefault();
                            router.push('/profile');
                          }}
                        >
                          Data Diri
                        </a>
                      )}
                    </Menu.Item>
                    <Menu.Item>
                      {({ active }) => (
                        <a
                          href="#"
                          className={classNames(
                            active ? 'bg-gray-100' : '',
                            'block px-4 py-2 text-sm text-gray-700',
                          )}
                        >
                          Ganti Password
                        </a>
                      )}
                    </Menu.Item>
                    <Menu.Item>
                      {({ active }) => (
                        <a
                          href="#"
                          className={classNames(
                            active ? 'bg-gray-100' : '',
                            'block px-4 py-2 text-sm text-gray-700',
                          )}
                          onClick={(e) => {
                            e.preventDefault();
                            setOpenModalLogout(true);
                          }}
                        >
                          Keluar
                        </a>
                      )}
                    </Menu.Item>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          )}

          {displayPublicMenu && (
            <>
              {NAVBAR_PUBLIC.map((item) => (
                <MenuItem key={item.route} to={item.route}>
                  {item.label}
                </MenuItem>
              ))}
              <MenuItem to="/login">Masuk</MenuItem>
              <div className="w-full">
                <Link href="/register" passHref>
                  <a className="text-white text-xl font-semibold bg-cafund-primary px-8 py-2 rounded-md">
                    Daftar
                  </a>
                </Link>
              </div>
            </>
          )}
        </div>
      </div>

      {openModalLogout && (
        <ModalLogout
          isOpen={openModalLogout}
          handleSubmit={logout}
          closeModal={() => setOpenModalLogout(false)}
        />
      )}
    </>
  );
};

const NavbarMenu = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  return (
    <div>
      <MenuToggle toggle={toggle} isOpen={isOpen} />
      <MenuLinks isOpen={isOpen} />
    </div>
  );
};

export default NavbarMenu;
