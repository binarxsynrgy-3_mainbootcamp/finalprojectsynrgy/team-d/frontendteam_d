import Logo from '@/components/Logo';
import NavbarMenu from '@/components/NavbarMenu';

const NavBar = () => (
  <nav className="flex flex-row justify-center bg-white">
    <div className="flex flex-row items-center justify-between flex-wrap w-full max-w-7xl">
      <Logo color={['white', 'white', 'primary.500', 'primary.500']} />
      <NavbarMenu />
    </div>
  </nav>
);

export default NavBar;
