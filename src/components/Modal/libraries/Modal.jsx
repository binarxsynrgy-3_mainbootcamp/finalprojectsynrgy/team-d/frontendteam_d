import useModalDispatch from '@/redux/modal/dispatch';
import { useState } from 'react';

const Modal = ({
  title,
  children,
  handleSubmit,
  singleButton = false,
  submitTitle = 'Submit',
}) => {
  const { doHideModal } = useModalDispatch();
  const [isLoading, setIsLoading] = useState(false);

  return (
    <>
      <div
        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 outline-none focus:outline-none"
        style={{ zIndex: 50 }}
      >
        <div
          className="relative my-6 mx-auto bg-cafund-base"
          style={{ minWidth: '500px', zIndex: 1 }}
        >
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full outline-none focus:outline-none">
            {title && (
              <div className="flex items-start justify-between p-5 border-blueGray-200 rounded-t">
                <h3 className="text-2xl text-cafund-primary font-semibold">
                  {title ?? 'Information'}
                </h3>
                <button
                  className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                  onClick={doHideModal}
                >
                  <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                    ×
                  </span>
                </button>
              </div>
            )}
            <div className="relative p-6 flex-auto">{children}</div>
            {singleButton ? (
              <div className="flex items-center justify-center gap-2 p-4 border-blueGray-200 rounded-b">
                <button
                  className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40 disabled:bg-gray-400"
                  type="button"
                  onClick={() => {
                    setIsLoading(true);
                    setTimeout(() => {
                      setIsLoading(false);
                      handleSubmit();
                    }, 3000);
                  }}
                  disabled={isLoading}
                >
                  {isLoading ? 'Submitting...' : submitTitle}
                </button>
              </div>
            ) : (
              <div className="flex items-center justify-end gap-2 p-4 border-blueGray-200 rounded-b">
                <button
                  className="bg-white font-bold py-2 px-4 rounded-md w-40"
                  type="button"
                  onClick={doHideModal}
                >
                  Close
                </button>
                <button
                  className="bg-cafund-primary text-white font-bold py-2 px-4 rounded-md w-40"
                  type="button"
                  onClick={() => {
                    setIsLoading(true);
                    setTimeout(() => {
                      setIsLoading(false);
                      handleSubmit();
                    }, 3000);
                  }}
                  disabled={isLoading}
                >
                  {isLoading ? 'Submitting...' : 'Submit'}
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  );
};

export default Modal;
