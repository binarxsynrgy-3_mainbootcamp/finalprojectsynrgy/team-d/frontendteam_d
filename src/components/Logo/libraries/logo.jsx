import Image from 'next/image';
import Link from 'next/link';

const Logo = () => (
  <div className="w-60">
    <Link href="/" className="w-full cursor-pointer" passHref>
      <div className="h-20 w-full relative cursor-pointer">
        <Image
          alt="Mountains"
          src="/logo.png"
          layout="fill"
          objectFit="contain"
        />
      </div>
    </Link>
  </div>
);

export default Logo;
