import Link from 'next/link';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import { BORROWER_SIDEBAR } from '@/lib/constant/menu';
import { removeLocalStorageValue } from '@/lib/hooks/useStorage';

const menus = BORROWER_SIDEBAR;

const Sidebar = () => {
  const router = useRouter();

  const resetUserAcitivty = () => {
    removeLocalStorageValue('assesment');
    removeLocalStorageValue('myClass');
    window.location.reload();
  };

  return (
    <div className="bg-white relative rounded-lg">
      <div className="xl:py-2">
        {menus &&
          menus.length > 0 &&
          menus.map((menu) => (
            <div key={menu.title} className="relative">
              <div
                className={classNames(
                  'text-gray-500 hover:text-cafund-primary',
                  {
                    'text-cafund-primary': router.pathname === menu.href,
                  },
                )}
              >
                <Link href={menu.href} passHref className="m-auto w-full">
                  <a className="flex items-start gap-4 shadow-light py-6 px-10  ">
                    <menu.icon className="h-7 w-7" />
                    <div className="text-md hover:font-semibold ">
                      {menu.title}
                    </div>
                  </a>
                </Link>
              </div>
            </div>
          ))}
        <div
          className="m-auto w-full cursor-pointer text-red-500 hover:text-red-400"
          role="button"
          tabIndex={0}
          onClick={resetUserAcitivty}
        >
          <a className="flex items-start gap-4 shadow-light py-6 px-10">
            <div className="text-md">Reset (dev only)</div>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
