module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    jest: true,
  },
  extends: ['plugin:react/recommended', 'airbnb', 'next/core-web-vitals'],
  settings: {
    'import/resolver': {
      typescript: {}, // this loads <rootdir>/tsconfig.json to eslint
    },
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'operator-linebreak': 'off',
    'react/jsx-props-no-spreading': 0,
    'import/no-unresolved': 0,
    'import/extensions': 0,
    'no-use-before-define': 'warn',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'object-curly-spacing': 'off',
    'object-curly-newline': 'off',
    'jsx-a11y/label-has-associated-control': 0,
    'comma-dangle': [2, 'always-multiline'],
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['hrefLeft', 'hrefRight'],
        aspects: ['invalidHref', 'preferButton'],
      },
    ],
  },
};
